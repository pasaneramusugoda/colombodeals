package com.syncbridge.colombodeals.common;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;

/**
 * Created by Pasan Eramusugoda on 4/23/2016.
 */
public abstract class HidingScrollListener extends RecyclerView.OnScrollListener {

    int visibleItemCount, totalItemCount;
    private int mToolbarOffset = 0;
    private int mToolbarHeight;
    private int previousTotal = 0; // The total number of items in the dataset after the last load
    private boolean loading = true; // True if we are still waiting for the last set of data to load.
    private int visibleThreshold = 2; // The minimum amount of items to have below your current scroll position before loading more.
    private int current_page = 0;

    private StaggeredGridLayoutManager mGridLayoutManager;

    public HidingScrollListener(Context context, int toolbarHeight, StaggeredGridLayoutManager gridLayoutManager) {
        Log.d("toolBarHeight", String.valueOf(toolbarHeight));
        this.mToolbarHeight = toolbarHeight;
        this.mGridLayoutManager = gridLayoutManager;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        //region hide
        clipToolbarOffset();
        onMoved(mToolbarOffset);

        if ((mToolbarOffset < mToolbarHeight && dy > 0) || (mToolbarOffset > 0 && dy < 0)) {
            mToolbarOffset += dy;
        }
        //endregion
        //region load more
        int[] firstVisibleItems = null;
        visibleItemCount = recyclerView.getChildCount();
        totalItemCount = mGridLayoutManager.getItemCount();
        firstVisibleItems = mGridLayoutManager.findFirstVisibleItemPositions(firstVisibleItems);

        if (loading) {
            if (totalItemCount > previousTotal) {
                loading = false;
                previousTotal = totalItemCount;
            }
        }
        if (!loading
                && (firstVisibleItems != null && firstVisibleItems.length > 0)
                && (totalItemCount - visibleItemCount) <= (firstVisibleItems[0] + visibleThreshold)) {
            // End has been reached

            // Do something
            current_page++;

            onLoadMore(current_page);

            loading = true;
        }
        //endregion
    }

    private void clipToolbarOffset() {
        if (mToolbarOffset > mToolbarHeight) {
            mToolbarOffset = mToolbarHeight;
        } else if (mToolbarOffset < 0) {
            mToolbarOffset = 0;
        }
    }

    public void resetCounts() {
        previousTotal = 0;
        current_page = 1;
    }

    public abstract void onMoved(int distance);

    public abstract void onLoadMore(int current_page);
}