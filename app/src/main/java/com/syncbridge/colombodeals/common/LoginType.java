package com.syncbridge.colombodeals.common;

/**
 * Created by Pasan Eramusugoda on 4/19/2016.
 */
public enum LoginType {
    NORMAL, FB, GOOGLE
}
