package com.syncbridge.colombodeals.common;

/**
 * Created by Pasan Eramusugoda on 4/8/2016.
 */
public enum Identity {
    HOME_PRODUCTS,
    BUSINESS_CARD_TEMPLATE,
    TRANSACTION,
    DEAL_ITEM, DEAL_ITEM_SHARE, NOTIFICATION, SLIDER, MERCHANT
}
