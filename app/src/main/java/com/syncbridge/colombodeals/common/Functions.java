package com.syncbridge.colombodeals.common;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Base64;
import android.view.View;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.rengwuxian.materialedittext.validation.METValidator;
import com.syncbridge.colombodeals.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by Pasan Eramusugoda on 4/8/2016.
 */
public class Functions {
    private static final String TAG = Functions.class.getSimpleName();

    public static boolean IsInternetOn(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null
                && cm.getActiveNetworkInfo().isAvailable()
                && cm.getActiveNetworkInfo().isConnected();
    }

    public static int GetColor(Context context, int color_resource) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return context.getColor(color_resource);
        } else {
            return context.getResources().getColor(color_resource);
        }
    }

    public static Float GetDimension(Context context, int int_resource) {
        return context.getResources().getDimension(int_resource);
    }

    public static Drawable GetDrawable(Context context, int drawable_resource) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return context.getDrawable(drawable_resource);
        } else {
            return context.getResources().getDrawable(drawable_resource);
        }
    }

    public static String GetStringResource(Context mContext, int mResourceId) {
        return mContext.getResources().getString(mResourceId);
    }

    public static String GetErrorMessage(@Nullable String message, int StatusCode, Throwable e, JSONObject errorResponse) {
        StringBuilder msg = new StringBuilder(message == null || message.isEmpty() ? "Please retry." : message);

        msg.append("\n");
        msg.append("\n");
        msg.append("Status Code: ").append(String.valueOf(StatusCode));
        if (e != null && e.getMessage() != null) {
            msg.append("\n");
            msg.append("Error: ").append(e.getMessage());
        }
        if (errorResponse != null && errorResponse.has("msg")) {
            try {
                msg.append("\n");
                msg.append("Error Response: ").append(errorResponse.getString("msg"));
            } catch (JSONException e1) {
                e1.printStackTrace();
            }
        }

        return msg.toString();
    }

    public static int GetToolbarHeight(Context context) {
        final TypedArray styledAttributes = context.getTheme().obtainStyledAttributes(
                new int[]{R.attr.actionBarSize});
        int toolbarHeight = (int) styledAttributes.getDimension(0, 0);
        styledAttributes.recycle();

        return toolbarHeight;
    }

    /**
     * Encodes the byte array into base64 string
     *
     * @param byteArray - byte array
     * @return String a {@link java.lang.String}
     */
    public static String EncodeBase64(byte[] byteArray) {
        return Base64.encodeToString(byteArray, Base64.NO_WRAP);
    }

    /**
     * Decodes the base64 string into byte array
     *
     * @param dataString - a {@link java.lang.String}
     * @return byte array
     */
    public static byte[] DecodeBase64(String dataString) {
        return Base64.decode(dataString, Base64.NO_WRAP);
    }

    //region date functions
    public static Date GetDateTime(String dateTime, String dateTimeFormat) {
        try {
            if (dateTimeFormat.isEmpty())
                dateTimeFormat = "yyyy-MM-dd";

            SimpleDateFormat retDateFormat = new SimpleDateFormat(dateTimeFormat, Locale.getDefault());
            retDateFormat.setTimeZone(TimeZone.getDefault());

            return retDateFormat.parse(dateTime);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static long GetDateTimeDifference(Date startDate, Date endDate) {

        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        Log.d(TAG, "startDate : " + startDate);
        Log.d(TAG, "endDate : " + endDate);
        Log.d(TAG, "different : " + different);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        Log.d(TAG, String.format(Locale.getDefault(),
                "%d days, %d hours, %d minutes, %d seconds%n",
                elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds));
        //return different;
        return elapsedMinutes;
    }
    //endregion

    //region animation

    public static void AnimateView(@NonNull Techniques techniques, @NonNull View view, int duration) {
        YoYo.with(techniques)
                .duration(duration)
                .playOn(view);
    }

    //endregion

    //region validation functions
    public static METValidator ValidationRequired(Context mContext) {
        return new METValidator(GetStringResource(mContext, R.string.error_required_field)) {
            @Override
            public boolean isValid(@NonNull CharSequence text, boolean isEmpty) {
                return !isEmpty && text.length() > 0;
            }
        };
    }

    public static METValidator ValidationMin(Context mContext, final int value) {
        return new METValidator(GetStringResource(mContext, R.string.error_phone_min_length)) {
            @Override
            public boolean isValid(@NonNull CharSequence text, boolean isEmpty) {
                return !isEmpty && text.length() >= value;
            }
        };
    }

    public static METValidator ValidationMax(Context mContext, final int value) {
        return new METValidator(GetStringResource(mContext, R.string.error_phone_max_length)) {
            @Override
            public boolean isValid(@NonNull CharSequence text, boolean isEmpty) {
                return !isEmpty && text.length() <= value;
            }
        };
    }

    public static METValidator ValidationEmail(Context mContext) {
        return new METValidator(Functions.GetStringResource(mContext, R.string.error_invalid_email)) {
            @Override
            public boolean isValid(@NonNull CharSequence text, boolean isEmpty) {
                return !isEmpty && IsValidEmail(text);
            }
        };
    }

    private static boolean IsValidEmail(CharSequence target) {
        return target != null && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static boolean IsValidPassword(CharSequence pass, CharSequence c_pass) {
        return !(pass == null || c_pass == null) && pass.toString().equals(c_pass.toString());
    }

    //endregion

    //region fire base deep link

    /**
     * Build a Firebase Dynamic Link.
     * https://firebase.google.com/docs/dynamic-links/android#create-a-dynamic-link
     *
     * @param deepLink   the deep link your app will open. This link must be a valid URL and use the
     *                   HTTP or HTTPS scheme.
     * @param minVersion the {@code versionCode} of the minimum version of your app that can open
     *                   the deep link. If the installed app is an older version, the user is taken
     *                   to the Play store to upgrade the app. Pass 0 if you do not
     *                   require a minimum version.
     * @param isAd       true if the dynamic link is used in an advertisement, false otherwise.
     * @return a {@link Uri} representing a properly formed deep link.
     */
    public static Uri BuildDeepLink(Context context, @NonNull Uri deepLink, int minVersion, boolean isAd) {
        // Get the unique appcode for this app.
        String appCode = context.getString(R.string.app_code);

        // Get this app's package name.
        String packageName = context.getPackageName();

        // Build the link with all required parameters
        Uri.Builder builder = new Uri.Builder()
                .scheme("https")
                .authority(appCode + ".app.goo.gl")
                .path("/")
                .appendQueryParameter("link", deepLink.toString())
                .appendQueryParameter("apn", packageName);

        // If the deep link is used in an advertisement, this value must be set to 1.
        if (isAd) {
            builder.appendQueryParameter("ad", "1");
        }

        // Minimum version is optional.
        if (minVersion > 0) {
            builder.appendQueryParameter("amv", Integer.toString(minVersion));
        }

        // Return the completed deep link.
        return builder.build();
    }

    public static void ShareDeepLink(Context context, String dealTitle, String deepLink) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, "ColomboDeals - " + dealTitle);
        intent.putExtra(Intent.EXTRA_TEXT, deepLink);

        context.startActivity(intent);
    }

    public static String GetAuthValue(Context context) {
        return "Basic " + EncodeBase64(GetStringResource(context, R.string.auth_key_value).getBytes());
    }

    //endregion
}
