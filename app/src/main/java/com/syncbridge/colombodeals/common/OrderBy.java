package com.syncbridge.colombodeals.common;

/**
 * Created by Pasan Eramusugoda on 5/13/2016.
 */
public enum OrderBy {
    expiry_dt, modified_at, name
}
