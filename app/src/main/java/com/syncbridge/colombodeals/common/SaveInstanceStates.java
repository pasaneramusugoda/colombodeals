package com.syncbridge.colombodeals.common;

/**
 * Created by Pasan Eramusugoda on 4/28/2016.
 */
public enum SaveInstanceStates {
    DEAL_ID, OFFSET, CATEGORY, QUERY, MERCHANT, START_DATE, END_DATE
}
