package com.syncbridge.colombodeals.common;

import android.view.View;

import com.daimajia.androidanimations.library.Techniques;

/**
 * Created by Pasan Eramusugoda on 4/8/2016.
 */
public class ProgressBar {

    private View mProgress;
    private int mRequestCount = 0;

    ProgressBar(View mProgress) {
        this.mProgress = mProgress;
    }

    public static ProgressBar getInstance(View mProgress) {
        return new ProgressBar(mProgress);
    }

    public void start() {
        mRequestCount++;

        if (mRequestCount > 0) {
            mProgress.setVisibility(View.VISIBLE);
            if (mProgress.getVisibility() != View.VISIBLE) {
                Functions.AnimateView(Techniques.FadeIn, mProgress, 500);
            }
        }
    }

    public void stop() {
        if (mRequestCount - 1 >= 0) {
            mRequestCount--;
        }

        if (mRequestCount == 0) {
            mProgress.setVisibility(View.GONE);
            if (mProgress.getVisibility() != View.GONE) {
                Functions.AnimateView(Techniques.FadeOut, mProgress, 2000);
            }
        }
    }

    public int getRequestCount() {
        return mRequestCount;
    }

    public View getView() {
        return mProgress;
    }
}
