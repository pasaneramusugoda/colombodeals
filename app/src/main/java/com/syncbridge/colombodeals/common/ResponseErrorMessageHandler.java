package com.syncbridge.colombodeals.common;

import android.content.Context;
import android.support.annotation.Nullable;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.crash.FirebaseCrash;
import com.syncbridge.colombodeals.R;

import org.json.JSONObject;

/**
 * Created by Pasan Eramusugoda on 4/8/2016.
 */
public class ResponseErrorMessageHandler implements MaterialDialog.SingleButtonCallback {

    private Context mContext;
    private MaterialDialog.SingleButtonCallback mCallbacks;
    private boolean mShowCancelButton;
    private boolean mCancelable;

    public ResponseErrorMessageHandler(Context context) {
        mContext = context;
        try {
            mCallbacks = (MaterialDialog.SingleButtonCallback) context;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement MaterialDialog.SingleButtonCallback.");
        }
    }

    public ResponseErrorMessageHandler(Context context, MaterialDialog.SingleButtonCallback callbacks) {
        mContext = context;
        mCallbacks = callbacks;
    }

    @Override
    public void onClick(MaterialDialog dialog, DialogAction which) {
        callback(dialog, which);
    }

    private void callback(MaterialDialog dialog, DialogAction which) {
        if (mCallbacks != null) {
            mCallbacks.onClick(dialog, which);
        }
    }

    public void showCancelButton(boolean showCancel) {
        mShowCancelButton = showCancel;
    }

    public void cancelable(boolean cancelable) {
        mCancelable = cancelable;
    }

    public void showRetry(@Nullable String message, int StatusCode, Throwable e, JSONObject errorResponse) {
        if (!mShowCancelButton) {
            try {
                new MaterialDialog.Builder(mContext)
                        .title(Functions.GetStringResource(mContext, R.string.dialog_title_error_response))
                        .content(Functions.GetErrorMessage(message, StatusCode, e, errorResponse))
                        .positiveText(R.string.dialog_button_retry)
                        .onPositive(this)
                        .cancelable(mCancelable)
                        .show();
            } catch (MaterialDialog.DialogException ex) {
                FirebaseCrash.report(ex);
            }
        } else {
            try {
                new MaterialDialog.Builder(mContext)
                        .title(Functions.GetStringResource(mContext, R.string.dialog_title_error_response))
                        .content(Functions.GetErrorMessage(message, StatusCode, e, errorResponse))
                        .positiveText(R.string.dialog_button_retry)
                        .onPositive(this)
                        .negativeText(R.string.dialog_button_cancel)
                        .onNegative(this)
                        .cancelable(mCancelable)
                        .show();
            } catch (MaterialDialog.DialogException ex) {
                FirebaseCrash.report(ex);
            }
        }
    }
}
