package com.syncbridge.colombodeals.fcm;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.syncbridge.colombodeals.api.ApiUsage;
import com.syncbridge.colombodeals.common.Log;
import com.syncbridge.colombodeals.common.Preference;
import com.syncbridge.colombodeals.models.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Pasan Eramusugoda on 5/19/2016.
 */
public class CDFBCInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = CDFBCInstanceIDService.class.getSimpleName();

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        sendRegistrationToServer(refreshedToken);
    }
    // [END refresh_token]

    /**
     * Persist token to third-party servers.
     * <p/>
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        User user = Preference.getInstance(getApplicationContext()).getCurrentUser();
        if (user != null) {
            ApiUsage.RegisterDevice(user.customer_id, token, new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        String jsonString = response.body().string();
                        Log.d(TAG, jsonString);
                        JSONObject mObject = new JSONObject(jsonString);
                        switch (mObject.getInt("status")) {
                            case 1:
                                Preference.getInstance(getApplicationContext()).write(Preference.Keys.INSTANCE_ID_SENT_TO_SERVER, true);
                                break;
                            case 0:
                                Preference.getInstance(getApplicationContext()).write(Preference.Keys.INSTANCE_ID_SENT_TO_SERVER, false);
                                Log.e(TAG, "RegisterDevice", mObject.getString("msg"));
                                break;
                        }
                    } catch (IOException | JSONException e) {
                        Log.e(TAG, e.toString());
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Preference.getInstance(getApplicationContext()).write(Preference.Keys.INSTANCE_ID_SENT_TO_SERVER, false);
                    Log.e(TAG, "sendRegistrationToServer", call.toString(), t);
                }
            });
        } else {
            Preference.getInstance(getApplicationContext()).write(Preference.Keys.INSTANCE_ID_SENT_TO_SERVER, false);
        }
    }
}