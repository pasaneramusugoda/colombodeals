package com.syncbridge.colombodeals.fcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.syncbridge.colombodeals.R;
import com.syncbridge.colombodeals.common.Log;
import com.syncbridge.colombodeals.common.Preference;
import com.syncbridge.colombodeals.controllers.AppController;
import com.syncbridge.colombodeals.events.NotificationEvent;
import com.syncbridge.colombodeals.models.Notification;
import com.syncbridge.colombodeals.views.activities.MainActivity;
import com.syncbridge.colombodeals.views.activities.NotificationActivity;

import org.greenrobot.eventbus.EventBus;

import java.util.Random;

/**
 * Created by Pasan Eramusugoda on 5/19/2016.
 */
public class CDFBCMessagingService extends FirebaseMessagingService {

    private static final String TAG = CDFBCMessagingService.class.getSimpleName();

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        String message;
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        if (remoteMessage.getNotification() != null && remoteMessage.getNotification().getBody() != null) {
            message = remoteMessage.getNotification().getBody();
            Log.d(TAG, "FCM Notification Message Body: " + message);
        } else {
            if (!remoteMessage.getData().isEmpty()) {
                Log.d(TAG, "Notification Message Data: " + remoteMessage.getData().toString());
            }

            try {
                if(remoteMessage.getData().get("type").equals("1")) {
                    int dealId = Integer.parseInt(remoteMessage.getData().get("deal_id"));
                    String merchantName = remoteMessage.getData().get("merchant");
                    String merchantLogo = remoteMessage.getData().get("logo");
                    message = remoteMessage.getData().get("message");

                    handleNotification(dealId, "Colombo Deals Promotion", merchantName, merchantLogo, message);
                } else {
                    message = remoteMessage.getData().get("message");

                    sendNotification(message);
                }
            } catch (NumberFormatException ex) {
                FirebaseCrash.report(ex);
            }
        }
    }
    // [END receive_message]

    private void handleNotification(int dealId, String messageTitle, String merchantName, String merchantLogo, String message) {

        Notification notification = new Notification();
        notification.setDealId(dealId);
        notification.setMessageTitle(messageTitle);
        notification.setMerchantName(merchantName);
        notification.setMerchantLogo(merchantLogo);
        notification.setMessage(message);
        notification.setIsRead(0);
        notification.save();

        if (AppController.isActivityInForeground()) {
            EventBus.getDefault().post(new NotificationEvent(notification));
        } else {
            sendNotification(notification);
        }
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     */
    private void sendNotification(String message) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_notification)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                .setContentTitle(getString(R.string.app_name))
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(generateRandom() /* ID of notification */, notificationBuilder.build());
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     */
    private void sendNotification(Notification notification) {
        Intent intent = new Intent(this, NotificationActivity.class);
        intent.putExtra(Preference.Keys.DEAL_ID.name(), String.valueOf(notification.getDealId()));
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_notification)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                .setContentTitle(notification.getMessageTitle())
                .setContentText(notification.getMessage())
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(generateRandom() /* ID of notification */, notificationBuilder.build());
    }

    public int generateRandom() {
        Random random = new Random();
        return random.nextInt(9999 - 1000) + 1000;
    }
}