package com.syncbridge.colombodeals.events;

import com.syncbridge.colombodeals.models.Notification;

/**
 * Created by Pasan Eramusugoda on 6/7/2016.
 */

public class NotificationEvent {
    public final Notification notification;

    public NotificationEvent(Notification notification) {
        this.notification = notification;
    }
}
