package com.syncbridge.colombodeals.db;

import com.raizlabs.android.dbflow.annotation.Database;

/**
 * Created by Pasan Eramusugoda on 6/7/2016.
 */

@Database(name = CDDatabase.NAME, version = CDDatabase.VERSION)
public class CDDatabase {
    public static final String NAME = "cd_db";
    public static final int VERSION = 1;
}
