package com.syncbridge.colombodeals.adapters;

import android.content.Context;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;
import com.syncbridge.colombodeals.R;
import com.syncbridge.colombodeals.common.Identity;
import com.syncbridge.colombodeals.interfaces.OnItemListener;
import com.syncbridge.colombodeals.models.Merchant;
import com.syncbridge.colombodeals.models.PaymentCard;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pasan Eramusugoda on 4/23/2016.
 */
public class MerchantAdapter extends RecyclerView.Adapter<MerchantAdapter.ViewHolder> {

    private static final String TAG = MerchantAdapter.class.getSimpleName();
    private Context mContext;
    private List<Merchant> mItems;

    private OnItemListener mListener;

    public MerchantAdapter(Context context, List<Merchant> items, OnItemListener listener) {
        mContext = context;
        mItems = items;
        mListener = listener;
    }

    @Override
    public MerchantAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_bank_item, null);
        return new MerchantAdapter.ViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(final MerchantAdapter.ViewHolder holder, final int position) {

        holder.itemView.setVisibility(View.GONE);

        final Merchant mObject = mItems.get(position);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onItemListener(position, Identity.MERCHANT, mObject);
            }
        });

        if (mObject.getLogo() != null && !mObject.getLogo().isEmpty()) {
            Picasso.with(mContext).load(mObject.getLogo()).into(holder.mBankLogo);
        }

        holder.mBankName.setText(mObject.getName());

        holder.mCheckbox.setChecked(mObject.isChecked());

        holder.itemView.setVisibility(View.VISIBLE);
    }

    @Override
    public int getItemCount() {
        return mItems == null ? 0 : mItems.size();
    }

    public void selectPosition(int position) {
        boolean isChecked = mItems.get(position).isChecked();
        mItems.get(position).setChecked(!isChecked);
        notifyDataSetChanged();
    }

    public void unselectAll() {
        for (Merchant m:
                mItems) {
            m.setChecked(false);
        }
        notifyDataSetChanged();
    }

    public List<String> getSelectedMerchantNames() {
        List<String> result = new ArrayList<>();

        for (Merchant m:
             mItems) {
            if(m.isChecked())
                result.add(m.getName());
        }

        return result;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        AppCompatImageView mBankLogo;
        AppCompatTextView mBankName;
        AppCompatCheckBox mCheckbox;

        ViewHolder(View itemView) {
            super(itemView);
            mBankLogo = (AppCompatImageView) itemView.findViewById(R.id.bank_logo);
            mBankName = (AppCompatTextView) itemView.findViewById(R.id.bank_name);
            mCheckbox = (AppCompatCheckBox) itemView.findViewById(R.id.checkbox);
        }
    }
}
