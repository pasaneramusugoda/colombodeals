package com.syncbridge.colombodeals.adapters;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;
import com.syncbridge.colombodeals.R;
import com.syncbridge.colombodeals.models.PaymentCard;

import java.util.List;

/**
 * Created by Pasan Eramusugoda on 4/23/2016.
 */
public class CardDetailAdapter extends RecyclerView.Adapter<CardDetailAdapter.ViewHolder> {

    private static final String TAG = CardDetailAdapter.class.getSimpleName();
    private static Context mContext;
    private List<PaymentCard> mItems;

    public CardDetailAdapter(Context context, List<PaymentCard> items) {
        mContext = context;
        this.mItems = items;
    }

    @Override
    public CardDetailAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_card_detail_item, null);
        return new CardDetailAdapter.ViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(final CardDetailAdapter.ViewHolder holder, final int position) {

        holder.itemView.setVisibility(View.GONE);

        final PaymentCard mObject = mItems.get(position);

        if (mObject.getLogo() != null && !mObject.getLogo().isEmpty()) {
            Picasso.with(mContext).load(mObject.getLogo()).into(holder.mBankLogo);
        }

        if (mObject.getDiscountPercent() != null) {
            holder.mDiscount.setText(String.format("%s%s", mObject.getDiscountPercent(), "%"));
        }

        holder.mExpires.setText(mObject.getExpiryDt());

        holder.itemView.setVisibility(View.VISIBLE);
    }

    @Override
    public int getItemCount() {
        return mItems == null ? 0 : mItems.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public AppCompatImageView mBankLogo;
        public AppCompatTextView mDiscount;
        public AppCompatTextView mExpires;

        public ViewHolder(View itemView) {
            super(itemView);
            mBankLogo = (AppCompatImageView) itemView.findViewById(R.id.bank_logo);
            mDiscount = (AppCompatTextView) itemView.findViewById(R.id.discount);
            mExpires = (AppCompatTextView) itemView.findViewById(R.id.tv_expire);
        }
    }
}
