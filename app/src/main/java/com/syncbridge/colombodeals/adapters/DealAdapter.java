package com.syncbridge.colombodeals.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.squareup.picasso.Picasso;
import com.syncbridge.colombodeals.R;
import com.syncbridge.colombodeals.common.Identity;
import com.syncbridge.colombodeals.common.Log;
import com.syncbridge.colombodeals.interfaces.OnItemListener;
import com.syncbridge.colombodeals.models.Banner;
import com.syncbridge.colombodeals.models.Deal;
import com.syncbridge.colombodeals.views.custom.CustomSliderView;

import java.util.HashMap;
import java.util.List;

import io.techery.properratingbar.ProperRatingBar;

/**
 * Created by Pasan Eramusugoda on 4/23/2016.
 */
public class DealAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = DealAdapter.class.getSimpleName();
    private static final int SLIDER = 111;
    private static final int DEAL = 222;
    private static Context mContext;
    private List<Deal> mItems;
    private List<Banner> mBanners;

    private OnItemListener mListener;

    public DealAdapter(Context context, List<Banner> banners, List<Deal> items, OnItemListener listener) {
        mContext = context;
        this.mBanners = banners;
        this.mItems = items;
        mListener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case SLIDER:
                return new ViewHolderSlider(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_image_slider, null));
            case DEAL:
                return new ViewHolderDeal(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_deal_item, null));
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, String.valueOf(position));
        bundle.putString(FirebaseAnalytics.Param.ITEM_LOCATION_ID, String.valueOf(holder.getAdapterPosition()));
        FirebaseAnalytics.getInstance(mContext).logEvent(FirebaseAnalytics.Event.VIEW_ITEM_LIST, bundle);

        if (holder instanceof ViewHolderSlider) {
            //region slider
            ViewHolderSlider mHolder = (ViewHolderSlider) holder;

            HashMap<Integer, String> url_maps = new HashMap<>();

            if (mBanners != null) {
                int index = 0;
                for (Banner banner : mBanners) {
                    if (banner.getImage() != null) {
                        url_maps.put(index, banner.getImage());
                        Log.d(TAG, banner.getImage());
                        index++;
                    }
                }
            }

            if (!url_maps.isEmpty()) {
                for (int name : url_maps.keySet()) {
                    CustomSliderView sliderView = new CustomSliderView(mContext, name, mBanners.get(name), mListener);
                    sliderView.image(url_maps.get(name));
                    sliderView.setScaleType(BaseSliderView.ScaleType.CenterInside);
//                defaultSliderView.empty(R.drawable.image_placeholder);
//                defaultSliderView.error(R.drawable.image_placeholder);

                    mHolder.mSlider.addSlider(sliderView);
                }
                mHolder.mSlider.startAutoCycle();
            }

            //endregion
        } else if (holder instanceof ViewHolderDeal) {
            //region deal
            holder.itemView.setVisibility(View.GONE);
            final int mPosition = mBanners != null && mBanners.size() > 0 && position > 0 ? position - 1 : position;

            ViewHolderDeal mHolder = (ViewHolderDeal) holder;

            final Deal mObject = mItems.get(mPosition);

            mHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onItemListener(mPosition, Identity.DEAL_ITEM, mObject);
                }
            });

            mHolder.mShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onItemListener(mPosition, Identity.DEAL_ITEM_SHARE, mObject);
                }
            });

            if (mObject.getMerchantLogo() != null && !mObject.getMerchantLogo().isEmpty()) {
                Picasso.with(mContext).load(mObject.getMerchantLogo()).into(mHolder.mMerchantLogo);
            }

            mHolder.mTitle.setText(mObject.getTitle());
            mHolder.mMerchantName.setText(mObject.getMerchant());
            mHolder.mRatingBar.setRating(mObject.getRating().intValue());

            if (mObject.getPaymentCards() != null && mObject.getPaymentCards().size() > 0) {
                mHolder.mCardsRecyclerView.setAdapter(new CardAdapter(mContext, mObject.getPaymentCards()));
            }

            holder.itemView.setVisibility(View.VISIBLE);

            //endregion
        }
    }

    @Override
    public int getItemCount() {
        int count = 0;

        if (mBanners != null && mBanners.size() > 0)
            count++;

        if (mItems != null && mItems.size() > 0)
            count += mItems.size();

        return count;
    }

    @Override
    public int getItemViewType(int position) {
        return position == 0 && (mBanners != null && mBanners.size() > 0) ? SLIDER : DEAL;
    }

    public List<Banner> getBanners() {
        return mBanners;
    }

    public void setBanners(List<Banner> mBanners) {
        this.mBanners = mBanners;
    }

    public List<Deal> getItems() {
        return mItems;
    }

    public void setItems(List<Deal> mItems) {
        this.mItems = mItems;
    }

    class ViewHolderSlider extends RecyclerView.ViewHolder {

        public SliderLayout mSlider;

        public ViewHolderSlider(View itemView) {
            super(itemView);
            mSlider = (SliderLayout) itemView.findViewById(R.id.slider);
            mSlider.setPresetTransformer(SliderLayout.Transformer.ZoomOutSlide);
            mSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Top);
            mSlider.setCustomAnimation(new DescriptionAnimation());
            mSlider.setCustomIndicator((PagerIndicator) itemView.findViewById(R.id.custom_indicator));
            mSlider.setDuration(5000);
        }
    }

    class ViewHolderDeal extends RecyclerView.ViewHolder {

        public AppCompatTextView mTitle;
        public AppCompatImageView mMerchantLogo;
        public AppCompatTextView mMerchantName;
        public ProperRatingBar mRatingBar;
        public AppCompatImageButton mShare;
        public RecyclerView mCardsRecyclerView;

        public ViewHolderDeal(View itemView) {
            super(itemView);
            mTitle = (AppCompatTextView) itemView.findViewById(R.id.deal_title);
            mMerchantLogo = (AppCompatImageView) itemView.findViewById(R.id.merchant_logo);
            mMerchantName = (AppCompatTextView) itemView.findViewById(R.id.merchant_name);
            mRatingBar = (ProperRatingBar) itemView.findViewById(R.id.rating);
            mShare = (AppCompatImageButton) itemView.findViewById(R.id.share);
            mCardsRecyclerView = (RecyclerView) itemView.findViewById(R.id.cards_recycler_view);
            mCardsRecyclerView.setHasFixedSize(true);
            mCardsRecyclerView.setLayoutManager(new LinearLayoutManager(itemView.getContext(), LinearLayoutManager.HORIZONTAL, false));
        }
    }
}
