package com.syncbridge.colombodeals.adapters;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;
import com.syncbridge.colombodeals.R;
import com.syncbridge.colombodeals.common.Functions;
import com.syncbridge.colombodeals.models.Review;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import io.techery.properratingbar.ProperRatingBar;

/**
 * Created by Pasan Eramusugoda on 4/23/2016.
 */
public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.ViewHolder> {

    private static final String TAG = CommentAdapter.class.getSimpleName();
    private static Context mContext;
    private List<Review> mItems;

    public CommentAdapter(Context context, List<Review> items) {
        mContext = context;
        this.mItems = items;
    }

    @Override
    public CommentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_comment_item, null);
        return new CommentAdapter.ViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(final CommentAdapter.ViewHolder holder, final int position) {

        holder.itemView.setVisibility(View.GONE);

        final Review mObject = mItems.get(position);

        if (mObject.getImage() != null && !mObject.getImage().isEmpty()) {
            Picasso.with(mContext)
                    .load(mObject.getImage())
                    .error(Functions.GetDrawable(mContext, R.mipmap.ic_launcher))
                    .into(holder.mProfileImage);
        } else {
            holder.mProfileImage.setImageDrawable(Functions.GetDrawable(mContext, R.mipmap.ic_launcher));
        }

        holder.mName.setText(mObject.getAuthorNickName());
        holder.mComment.setText(mObject.getContent());
        holder.mRatingBar.setRating(mObject.getRating());

        holder.itemView.setVisibility(View.VISIBLE);
    }

    @Override
    public int getItemCount() {
        return mItems == null ? 0 : mItems.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public CircleImageView mProfileImage;
        public AppCompatTextView mName;
        public AppCompatTextView mComment;
        public ProperRatingBar mRatingBar;

        public ViewHolder(View itemView) {
            super(itemView);
            mProfileImage = (CircleImageView) itemView.findViewById(R.id.profile_image);
            mName = (AppCompatTextView) itemView.findViewById(R.id.tv_name);
            mComment = (AppCompatTextView) itemView.findViewById(R.id.tv_comment);
            mRatingBar = (ProperRatingBar) itemView.findViewById(R.id.rating);
        }
    }
}
