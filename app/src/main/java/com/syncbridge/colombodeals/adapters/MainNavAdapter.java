package com.syncbridge.colombodeals.adapters;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.syncbridge.colombodeals.R;
import com.syncbridge.colombodeals.common.Functions;
import com.syncbridge.colombodeals.interfaces.NavigationDrawerCallbacks;
import com.syncbridge.colombodeals.models.MainNavItem;

import java.util.List;

/**
 * Created by Pasan Eramusugoda on 4/12/2016.
 */
public class MainNavAdapter extends RecyclerView.Adapter<MainNavAdapter.ViewHolder> {
    private static String TAG = MainNavAdapter.class.getSimpleName();
    private List<MainNavItem> mData;
    private NavigationDrawerCallbacks mDrawerCallbacks;
    private int mSelectedPosition = -1;

    public MainNavAdapter(List<MainNavItem> data) {
        mData = data;
    }


    public void setNavigationDrawerCallbacks(NavigationDrawerCallbacks drawerCallbacks) {
        mDrawerCallbacks = drawerCallbacks;
    }

    @Override
    public MainNavAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_nav_drawer_item, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MainNavAdapter.ViewHolder viewHolder, final int i) {
        viewHolder.textView.setText(mData.get(i).getText());
        if (mData.get(i).getDrawable() > 0) {
            viewHolder.imageView.setImageDrawable(Functions.GetDrawable(viewHolder.context, mData.get(i).getDrawable()));
        } else {
            viewHolder.imageView.setVisibility(View.GONE);
        }
//        viewHolder.textView.setTypeface(Functions.GetTypeFace(viewHolder.itemView.getContext(), FontType.MontserratLight));
//        viewHolder.textView.setCompoundDrawablesWithIntrinsicBounds(mData.get(i).getDrawable(), null, null, null);

        if (mData.get(i).isEnable()) {
            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mDrawerCallbacks != null)
                        mDrawerCallbacks.onNavigationDrawerItemSelected(i);
                }
            });

            viewHolder.textView.setTextColor(viewHolder.itemView.getContext().getResources().getColor(R.color.colorText2));
//            int mTouchedPosition = -1;
//            if (mSelectedPosition == i || mTouchedPosition == i) {
//                viewHolder.textView.setTextColor(viewHolder.itemView.getContext().getResources().getColor(R.color.colorText1));
//            } else {
//                viewHolder.textView.setTextColor(viewHolder.itemView.getContext().getResources().getColor(R.color.colorText2));
//            }
        } else {
            viewHolder.textView.setTextColor(viewHolder.itemView.getContext().getResources().getColor(R.color.colorTextDisabled));
        }
    }

    @Override
    public int getItemCount() {
        return mData != null ? mData.size() : 0;
    }

    public void selectPosition(int position) {
        int lastPosition = mSelectedPosition;
        mSelectedPosition = position;
        notifyItemChanged(lastPosition);
        notifyItemChanged(position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public Context context;
        public AppCompatImageView imageView;
        public AppCompatTextView textView;

        public ViewHolder(View itemView) {
            super(itemView);
            context = itemView.getContext();
            imageView = (AppCompatImageView) itemView.findViewById(R.id.nav_icon);
            textView = (AppCompatTextView) itemView.findViewById(R.id.nav_item);
        }
    }
}
