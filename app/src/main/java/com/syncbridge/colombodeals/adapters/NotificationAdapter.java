package com.syncbridge.colombodeals.adapters;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.daimajia.androidanimations.library.Techniques;
import com.squareup.picasso.Picasso;
import com.syncbridge.colombodeals.R;
import com.syncbridge.colombodeals.common.Functions;
import com.syncbridge.colombodeals.common.Identity;
import com.syncbridge.colombodeals.interfaces.OnItemListener;
import com.syncbridge.colombodeals.models.Notification;

import java.util.List;

/**
 * Created by Pasan Eramusugoda on 6/7/2016.
 */
public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {

    private Context mContext;
    private List<Notification> mItems;

    private OnItemListener mListener;

    public NotificationAdapter(Context context, List<Notification> items, OnItemListener listener) {
        mContext = context;
        this.mItems = items;
        mListener = listener;
    }

    @Override
    public NotificationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new NotificationAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_notification_item, null));
    }

    @Override
    public void onBindViewHolder(final NotificationAdapter.ViewHolder holder, final int position) {

        holder.itemView.setVisibility(View.GONE);

        final Notification mObject = mItems.get(position);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onItemListener(position, Identity.NOTIFICATION, mObject);
            }
        });

        if (mObject.getMerchantLogo() != null && !mObject.getMerchantLogo().isEmpty()) {
            Picasso.with(mContext).load(mObject.getMerchantLogo()).into(holder.mMerchantLogo);
        }

        holder.mTitle.setText(mObject.getMessage());
        holder.mMerchantName.setText(mObject.getMerchantName());

        holder.itemView.setVisibility(View.VISIBLE);

        if (mObject.getIsRead() == 0)
            Functions.AnimateView(Techniques.Shake, holder.itemView, 1000);
    }

    @Override
    public int getItemCount() {
        return mItems != null ? mItems.size() : 0;
    }

    public List<Notification> getItems() {
        return mItems;
    }

    public void setItems(List<Notification> mItems) {
        this.mItems = mItems;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        AppCompatTextView mTitle;
        AppCompatImageView mMerchantLogo;
        AppCompatTextView mMerchantName;

        ViewHolder(View itemView) {
            super(itemView);
            mTitle = (AppCompatTextView) itemView.findViewById(R.id.deal_title);
            mMerchantLogo = (AppCompatImageView) itemView.findViewById(R.id.merchant_logo);
            mMerchantName = (AppCompatTextView) itemView.findViewById(R.id.merchant_name);
        }
    }
}
