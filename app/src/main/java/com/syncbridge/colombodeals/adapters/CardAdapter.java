package com.syncbridge.colombodeals.adapters;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;
import com.syncbridge.colombodeals.R;
import com.syncbridge.colombodeals.models.PaymentCard;

import java.util.List;

/**
 * Created by Pasan Eramusugoda on 4/23/2016.
 */
public class CardAdapter extends RecyclerView.Adapter<CardAdapter.ViewHolder> {

    private Context mContext;
    private List<PaymentCard> mItems;

    CardAdapter(Context context, List<PaymentCard> items) {
        mContext = context;
        this.mItems = items;
    }

    @Override
    public CardAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_card_item, null);
        return new CardAdapter.ViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(final CardAdapter.ViewHolder holder, final int position) {

        holder.itemView.setVisibility(View.GONE);

        final PaymentCard mObject = mItems.get(position);

        if (mObject.getLogo() != null && !mObject.getLogo().isEmpty()) {
            Picasso.with(mContext).load(mObject.getLogo()).into(holder.mBankLogo);
        }

        if (mObject.getDiscountPercent() != null) {
            holder.mDiscount.setText(String.format("%s%s", mObject.getDiscountPercent(), "%"));
        }
        holder.itemView.setVisibility(View.VISIBLE);
    }

    @Override
    public int getItemCount() {
        return mItems == null ? 0 : mItems.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        AppCompatImageView mBankLogo;
        AppCompatTextView mDiscount;

        ViewHolder(View itemView) {
            super(itemView);
            mBankLogo = (AppCompatImageView) itemView.findViewById(R.id.bank_logo);
            mDiscount = (AppCompatTextView) itemView.findViewById(R.id.discount);
        }
    }
}
