package com.syncbridge.colombodeals.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.syncbridge.colombodeals.R;
import com.syncbridge.colombodeals.views.fragments.AllDealsFragment;
import com.syncbridge.colombodeals.views.fragments.DealsFragment;

/**
 * Created by Pasan Eramusugoda on 5/26/2016.
 */
public class SectionsPagerAdapter extends FragmentPagerAdapter {

    private Context mContext;
    private String mTabTitles[] = new String[]{"All Deals", "Restaurant", "Leisure", "Travel", "Fashion", "Lifestyle"};
    private int[] mImageResId = {
            R.drawable.all_deals,
            R.drawable.restaurant,
            R.drawable.leisure,
            R.drawable.travel,
            R.drawable.fashion,
            R.drawable.lifestyle
    };

    public SectionsPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return AllDealsFragment.newInstance();
            default:
                return DealsFragment.newInstance(mTabTitles[position].toLowerCase());
        }
    }

    @Override
    public int getCount() {
        return mTabTitles.length;
    }

    public View getTabView(int position) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.layout_custom_tab, null);
        TextView tv = (TextView) v.findViewById(R.id.tab_title);
        tv.setText(mTabTitles[position]);
        ImageView img = (ImageView) v.findViewById(R.id.tab_image);
        img.setImageResource(mImageResId[position]);
        return v;
    }
}
