package com.syncbridge.colombodeals.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pasan Eramusugoda on 4/23/2016.
 */
public class Deals {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("deals")
    @Expose
    private List<Deal> deals = new ArrayList<Deal>();

    /**
     * No args constructor for use in serialization
     */
    public Deals() {
    }

    /**
     * @param status
     * @param deals
     * @param msg
     */
    public Deals(Integer status, String msg, List<Deal> deals) {
        this.status = status;
        this.msg = msg;
        this.deals = deals;
    }

    /**
     * @return The status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * @return The msg
     */
    public String getMsg() {
        return msg;
    }

    /**
     * @param msg The msg
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     * @return The deals
     */
    public List<Deal> getDeals() {
        return deals;
    }

    /**
     * @param deals The deals
     */
    public void setDeals(List<Deal> deals) {
        this.deals = deals;
    }

}
