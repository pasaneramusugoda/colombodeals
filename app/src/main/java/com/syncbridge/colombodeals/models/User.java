package com.syncbridge.colombodeals.models;

import com.syncbridge.colombodeals.common.LoginType;

/**
 * Created by Pasan Eramusugoda on 4/19/2016.
 */
public class User {
    public String name;
    public String email;
    public String customer_id;
    public String user_id;
    public String gender;
    public String token;
    public String token_expire_at;
    public LoginType login_type;
    public String profile_pic_url;

    public User() {
        super();
    }
}
