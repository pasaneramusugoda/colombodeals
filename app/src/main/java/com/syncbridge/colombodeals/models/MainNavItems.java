package com.syncbridge.colombodeals.models;

/**
 * Created by Pasan Eramusugoda on 4/12/2016.
 */
public class MainNavItems {
    public class MY_ACCOUNT {
        public static final int id = 0;
        public static final String name = "MY_ACCOUNT";
        public static final String title = "My Account";
    }

    public class SUBSCRIBE {
        public static final int id = 1;
        public static final String name = "SUBSCRIBE";
        public static final String title = "Subscribe";
    }

    public class NOTIFICATION {
        public static final int id = 2;
        public static final String name = "NOTIFICATION";
        public static final String title = "Notification";
    }

    public class CONTACT_US {
        public static final int id = 3;
        public static final String name = "CONTACT_US";
        public static final String title = "Contact Us";
    }

    public class ABOUT_US {
        public static final int id = 4;
        public static final String name = "ABOUT_US";
        public static final String title = "About Us";
    }

    public class LOG_OUT {
        public static final int id = 5;
        public static final String name = "LOG_OUT";
        public static final String title = "Log Out";
    }
}
