package com.syncbridge.colombodeals.models;

import com.google.android.gms.maps.model.Marker;

/**
 * Created by Pasan Eramusugoda on 5/27/2016.
 */
public class MarkerObject {

    private Marker marker;
    private String dealId;

    public MarkerObject() {
        super();
    }

    public MarkerObject(Marker marker, String dealId) {
        this.marker = marker;
        this.dealId = dealId;
    }

    public Marker getMarker() {
        return marker;
    }

    public void setMarker(Marker marker) {
        this.marker = marker;
    }

    public String getDealId() {
        return dealId;
    }

    public void setDealId(String dealId) {
        this.dealId = dealId;
    }
}
