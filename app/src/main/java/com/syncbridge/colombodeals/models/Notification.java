package com.syncbridge.colombodeals.models;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;
import com.syncbridge.colombodeals.db.CDDatabase;

/**
 * Created by Pasan Eramusugoda on 6/7/2016.
 */

@Table(database = CDDatabase.class)
public class Notification extends BaseModel {

    @Column
    @PrimaryKey(autoincrement = true)
    int id;

    @Column
    int dealId;

    @Column
    String messageTitle;

    @Column
    String merchantName;

    @Column
    String merchantLogo;

    @Column
    String message;

    @Column
    int isRead;

    public Notification() {
        super();
    }

    public int getIsRead() {
        return isRead;
    }

    public void setIsRead(int isRead) {
        this.isRead = isRead;
    }

    public int getDealId() {
        return dealId;
    }

    public void setDealId(int dealId) {
        this.dealId = dealId;
    }

    public String getMessageTitle() {
        return messageTitle;
    }

    public void setMessageTitle(String messageTitle) {
        this.messageTitle = messageTitle;
    }

    public String getMerchantLogo() {
        return merchantLogo;
    }

    public void setMerchantLogo(String merchantLogo) {
        this.merchantLogo = merchantLogo;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}