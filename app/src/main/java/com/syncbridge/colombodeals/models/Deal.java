package com.syncbridge.colombodeals.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pasan Eramusugoda on 4/23/2016.
 */
public class Deal implements Comparable<Deal> {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("highlights")
    @Expose
    private String highlights;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("lat")
    @Expose
    private Double lat;
    @SerializedName("lng")
    @Expose
    private Double lon;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("web")
    @Expose
    private String web;
    @SerializedName("is_featured")
    @Expose
    private String isFeatured;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("merchant")
    @Expose
    private String merchant;
    @SerializedName("created_at")
    @Expose
    private Object createdAt;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("merchant_logo")
    @Expose
    private String merchantLogo;
    @SerializedName("payment_cards")
    @Expose
    private List<PaymentCard> paymentCards = new ArrayList<PaymentCard>();
    @SerializedName("reviews")
    @Expose
    private List<Review> reviews = new ArrayList<Review>();
    @SerializedName("rating")
    @Expose
    private Double rating;

    /**
     * No args constructor for use in serialization
     */
    public Deal() {
    }

    /**
     * @param merchantLogo
     * @param paymentCards
     * @param location
     * @param merchant
     * @param isFeatured
     * @param image
     * @param lon
     * @param highlights
     * @param id
     * @param title
     * @param category
     * @param createdAt
     * @param lat
     */
    public Deal(String id, String title, String highlights, String location, Double lat, Double lon, String phone, String web, String isFeatured, String category, String merchant, Object createdAt, String image, String merchantLogo, List<PaymentCard> paymentCards, List<Review> reviews, Double rating) {
        this.id = id;
        this.title = title;
        this.highlights = highlights;
        this.location = location;
        this.lat = lat;
        this.lon = lon;
        this.phone = phone;
        this.web = web;
        this.isFeatured = isFeatured;
        this.category = category;
        this.merchant = merchant;
        this.createdAt = createdAt;
        this.image = image;
        this.merchantLogo = merchantLogo;
        this.paymentCards = paymentCards;
        this.reviews = reviews;
        this.rating = rating;
    }

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return The highlights
     */
    public String getHighlights() {
        return highlights;
    }

    /**
     * @param highlights The highlights
     */
    public void setHighlights(String highlights) {
        this.highlights = highlights;
    }

    /**
     * @return The location
     */
    public String getLocation() {
        return location;
    }

    /**
     * @param location The location
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * @return The lat
     */
    public Double getLat() {
        return lat;
    }

    /**
     * @param lat The lat
     */
    public void setLat(Double lat) {
        this.lat = lat;
    }

    /**
     * @return The lon
     */
    public Double getLon() {
        return lon;
    }

    /**
     * @param lon The lon
     */
    public void setLon(Double lon) {
        this.lon = lon;
    }

    /**
     * @return The phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone The phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return The web
     */
    public String getWeb() {
        return web;
    }

    /**
     * @param web The web
     */
    public void setWeb(String web) {
        this.web = web;
    }

    /**
     * @return The isFeatured
     */
    public String getIsFeatured() {
        return isFeatured;
    }

    /**
     * @param isFeatured The is_featured
     */
    public void setIsFeatured(String isFeatured) {
        this.isFeatured = isFeatured;
    }

    /**
     * @return The category
     */
    public String getCategory() {
        return category;
    }

    /**
     * @param category The category
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * @return The merchant
     */
    public String getMerchant() {
        return merchant;
    }

    /**
     * @param merchant The merchant
     */
    public void setMerchant(String merchant) {
        this.merchant = merchant;
    }

    /**
     * @return The createdAt
     */
    public Object getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt The created_at
     */
    public void setCreatedAt(Object createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return The image
     */
    public String getImage() {
        return image;
    }

    /**
     * @param image The image
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * @return The merchantLogo
     */
    public String getMerchantLogo() {
        return merchantLogo;
    }

    /**
     * @param merchantLogo The merchant_logo
     */
    public void setMerchantLogo(String merchantLogo) {
        this.merchantLogo = merchantLogo;
    }

    /**
     * @return The paymentCards
     */
    public List<PaymentCard> getPaymentCards() {
        return paymentCards;
    }

    /**
     * @param paymentCards The payment_cards
     */
    public void setPaymentCards(List<PaymentCard> paymentCards) {
        this.paymentCards = paymentCards;
    }

    /**
     * @return The reviews
     */
    public List<Review> getReviews() {
        return reviews;
    }

    /**
     * @param reviews The reviews
     */
    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }

    /**
     * @return The rating
     */
    public Double getRating() {
        return rating;
    }

    /**
     * @param rating The rating
     */
    public void setRating(Double rating) {
        this.rating = rating;
    }

    @Override
    public int compareTo(Deal another) {
        return getId().compareTo(another.getId());
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof Deal && ((Deal) o).getId().equals(getId());
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }
}
