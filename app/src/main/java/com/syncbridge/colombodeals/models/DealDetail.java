package com.syncbridge.colombodeals.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Pasan Eramusugoda on 4/25/2016.
 */
public class DealDetail {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("deal")
    @Expose
    private Deal deal;

    /**
     * No args constructor for use in serialization
     */
    public DealDetail() {
    }

    /**
     * @param deal
     * @param status
     * @param msg
     */
    public DealDetail(Integer status, String msg, Deal deal) {
        this.status = status;
        this.msg = msg;
        this.deal = deal;
    }

    /**
     * @return The status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * @return The msg
     */
    public String getMsg() {
        return msg;
    }

    /**
     * @param msg The msg
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     * @return The deal
     */
    public Deal getDeal() {
        return deal;
    }

    /**
     * @param deal The deal
     */
    public void setDeal(Deal deal) {
        this.deal = deal;
    }

}
