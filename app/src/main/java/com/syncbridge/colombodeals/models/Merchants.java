
package com.syncbridge.colombodeals.models;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Merchants {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("merchants")
    @Expose
    private List<Merchant> merchants = new ArrayList<Merchant>();

    /**
     * 
     * @return
     *     The status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 
     * @return
     *     The msg
     */
    public String getMsg() {
        return msg;
    }

    /**
     * 
     * @param msg
     *     The msg
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     * 
     * @return
     *     The merchants
     */
    public List<Merchant> getMerchants() {
        return merchants;
    }

    /**
     * 
     * @param merchants
     *     The merchants
     */
    public void setMerchants(List<Merchant> merchants) {
        this.merchants = merchants;
    }

}
