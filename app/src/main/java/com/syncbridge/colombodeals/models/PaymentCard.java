package com.syncbridge.colombodeals.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Pasan Eramusugoda on 4/23/2016.
 */
public class PaymentCard {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("discount_percent")
    @Expose
    private String discountPercent;
    @SerializedName("start_dt")
    @Expose
    private String startDt;
    @SerializedName("expiry_dt")
    @Expose
    private String expiryDt;
    @SerializedName("logo")
    @Expose
    private String logo;

    /**
     * No args constructor for use in serialization
     */
    public PaymentCard() {
    }

    /**
     * @param logo
     * @param expiryDt
     * @param startDt
     * @param name
     * @param discountPercent
     */
    public PaymentCard(String name, String discountPercent, String startDt, String expiryDt, String logo) {
        this.name = name;
        this.discountPercent = discountPercent;
        this.startDt = startDt;
        this.expiryDt = expiryDt;
        this.logo = logo;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The discountPercent
     */
    public String getDiscountPercent() {
        return discountPercent;
    }

    /**
     * @param discountPercent The discount_percent
     */
    public void setDiscountPercent(String discountPercent) {
        this.discountPercent = discountPercent;
    }

    /**
     * @return The startDt
     */
    public String getStartDt() {
        return startDt;
    }

    /**
     * @param startDt The start_dt
     */
    public void setStartDt(String startDt) {
        this.startDt = startDt;
    }

    /**
     * @return The expiryDt
     */
    public String getExpiryDt() {
        return expiryDt;
    }

    /**
     * @param expiryDt The expiry_dt
     */
    public void setExpiryDt(String expiryDt) {
        this.expiryDt = expiryDt;
    }

    /**
     * @return The logo
     */
    public String getLogo() {
        return logo;
    }

    /**
     * @param logo The logo
     */
    public void setLogo(String logo) {
        this.logo = logo;
    }

}
