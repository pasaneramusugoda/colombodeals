package com.syncbridge.colombodeals.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pasan Eramusugoda on 4/23/2016.
 */
public class Banners {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("banners")
    @Expose
    private List<Banner> banners = new ArrayList<Banner>();

    /**
     * No args constructor for use in serialization
     */
    public Banners() {
    }

    /**
     * @param status
     * @param msg
     * @param banners
     */
    public Banners(Integer status, String msg, List<Banner> banners) {
        this.status = status;
        this.msg = msg;
        this.banners = banners;
    }

    /**
     * @return The status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * @return The msg
     */
    public String getMsg() {
        return msg;
    }

    /**
     * @param msg The msg
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     * @return The banners
     */
    public List<Banner> getBanners() {
        return banners;
    }

    /**
     * @param banners The banners
     */
    public void setBanners(List<Banner> banners) {
        this.banners = banners;
    }

}
