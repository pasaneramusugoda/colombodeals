package com.syncbridge.colombodeals.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Pasan Eramusugoda on 4/23/2016.
 */
public class Banner {

    @SerializedName("banner_id")
    @Expose
    private Integer bannerId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("deal_id")
    @Expose
    private String dealId;
    @SerializedName("image")
    @Expose
    private String image;

    /**
     * No args constructor for use in serialization
     */
    public Banner() {
    }

    /**
     * @param title
     * @param bannerId
     * @param image
     * @param dealId
     */
    public Banner(Integer bannerId, String title, String dealId, String image) {
        this.bannerId = bannerId;
        this.title = title;
        this.dealId = dealId;
        this.image = image;
    }

    /**
     * @return The bannerId
     */
    public Integer getBannerId() {
        return bannerId;
    }

    /**
     * @param bannerId The banner_id
     */
    public void setBannerId(Integer bannerId) {
        this.bannerId = bannerId;
    }

    /**
     * @return The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return The dealId
     */
    public String getDealId() {
        return dealId;
    }

    /**
     * @param dealId The deal_id
     */
    public void setDealId(String dealId) {
        this.dealId = dealId;
    }

    /**
     * @return The image
     */
    public String getImage() {
        return image;
    }

    /**
     * @param image The image
     */
    public void setImage(String image) {
        this.image = image;
    }

}
