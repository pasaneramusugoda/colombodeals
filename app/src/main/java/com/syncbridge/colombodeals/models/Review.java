package com.syncbridge.colombodeals.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Pasan Eramusugoda on 4/28/2016.
 */
public class Review {

    @SerializedName("author_nick_name")
    @Expose
    private String authorNickName;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("rating")
    @Expose
    private Integer rating;
    @SerializedName("created_at")
    @Expose
    private String createdAt;

    /**
     * No args constructor for use in serialization
     */
    public Review() {
    }

    /**
     * @param content
     * @param createdAt
     * @param authorNickName
     * @param rating
     */
    public Review(String authorNickName, String image, String content, Integer rating, String createdAt) {
        this.authorNickName = authorNickName;
        this.image = image;
        this.content = content;
        this.rating = rating;
        this.createdAt = createdAt;
    }

    /**
     * @return The authorNickName
     */
    public String getAuthorNickName() {
        return authorNickName;
    }

    /**
     * @param authorNickName The author_nick_name
     */
    public void setAuthorNickName(String authorNickName) {
        this.authorNickName = authorNickName;
    }

    /**
     * @return The image
     */
    public String getImage() {
        return image;
    }

    /**
     * @param image The image
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * @return The content
     */
    public String getContent() {
        return content;
    }

    /**
     * @param content The content
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * @return The rating
     */
    public Integer getRating() {
        return rating;
    }

    /**
     * @param rating The rating
     */
    public void setRating(Integer rating) {
        this.rating = rating;
    }

    /**
     * @return The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

}
