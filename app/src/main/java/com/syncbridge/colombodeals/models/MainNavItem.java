package com.syncbridge.colombodeals.models;

/**
 * Created by Pasan Eramusugoda on 4/12/2016.
 */
public class MainNavItem {
    private String mText;
    private int mDrawableResId;
    private int mNotificationCount;
    private boolean mEnable;

    public MainNavItem(String text, boolean enable) {
        mText = text;
        mEnable = enable;
    }

    public MainNavItem(String text, int drawableResId, boolean enable) {
        mText = text;
        mDrawableResId = drawableResId;
        mEnable = enable;
    }

    public MainNavItem(String text, int drawableResId, int notificationCount) {
        mText = text;
        mDrawableResId = drawableResId;
        mNotificationCount = notificationCount;
    }

    public int getNotificationCount() {
        return mNotificationCount;
    }

    public void setNotificationCount(int mNotificationCount) {
        this.mNotificationCount = mNotificationCount;
    }

    public String getText() {
        return mText;
    }

    public void setText(String text) {
        mText = text;
    }

    public int getDrawable() {
        return mDrawableResId;
    }

    public void setDrawable(int drawable) {
        mDrawableResId = drawable;
    }

    public boolean isEnable() {
        return mEnable;
    }

    public void setEnable(boolean mEnable) {
        this.mEnable = mEnable;
    }
}
