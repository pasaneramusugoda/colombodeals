package com.syncbridge.colombodeals.api;

import com.syncbridge.colombodeals.models.Banners;
import com.syncbridge.colombodeals.models.DealDetail;
import com.syncbridge.colombodeals.models.Deals;
import com.syncbridge.colombodeals.models.Merchants;

import java.util.HashMap;

import okhttp3.HttpUrl;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by Pasan Eramusugoda on 4/15/2016.
 */
public interface Api {
    @POST("auth/social_login")
    @FormUrlEncoded
    Call<ResponseBody> UserSocialLogin(
            @Header("Authorization") String value,
            @FieldMap HashMap<String, String> body
    );

    @POST("auth/login")
    @FormUrlEncoded
    Call<ResponseBody> UserLogin(
            @Header("Authorization") String value,
            @Field("email") String email,
            @Field("password") String password
    );

    @POST("auth/logout")
    @FormUrlEncoded
    Call<ResponseBody> UserLogout(
            @Header("Authorization") String value,
            @FieldMap HashMap<String, String> body
    );

    @POST("customers/create")
    @FormUrlEncoded
    Call<ResponseBody> UserCreate(
            @Header("Authorization") String value,
            @FieldMap HashMap<String, String> body
    );

    @POST("customers/update")
    @FormUrlEncoded
    Call<ResponseBody> UserUpdate(
            @Header("Authorization") String value,
            @FieldMap HashMap<String, String> body);

    @Headers("Cache-Control: max-age=640000")
    @GET("deals")
    Call<Deals> GetDeals(
            @Header("Authorization") String value,
            @Query("limit") Integer limit,
            @Query("offset") Integer offset
    );

    @Headers("Cache-Control: max-age=640000")
    @GET("deals/category")
    Call<Deals> GetDeals(
            @Header("Authorization") String value,
            @Query("category") String category,
            @Query("limit") Integer limit,
            @Query("offset") Integer offset
    );

    @GET
    Call<Deals> GetDeals(
            @Header("Authorization") String value,
            @Url HttpUrl url
//            @Query("merchant") String category,
//            @Query("start_dt") String start_date,
//            @Query("expiry_dt") String end_date
    );

    @GET("deals/nearby")
    Call<Deals> GetDealsNearby(
            @Header("Authorization") String value,
            @Query("lat") Double lat,
            @Query("lon") Double lon,
            @Query("radius") Integer radius
    );

    @GET("banners")
    Call<Banners> GetBanners(@Header("Authorization") String value);

    @GET("deals/single")
    Call<DealDetail> GetDeal(
            @Header("Authorization") String value,
            @Query("deal_id") String deal_id
    );

    @POST("deals/add_review")
    @FormUrlEncoded
    Call<ResponseBody> AddReview(
            @Header("Authorization") String value,
            @FieldMap HashMap<String, String> body
    );

    @GET("customers/profile")
    Call<ResponseBody> GetCustomer(
            @Header("Authorization") String value,
            @Query("customer_id") String customer_id,
            @Query("token") String token
    );

    @GET("coupons")
    Call<ResponseBody> GetCoupons(@Header("Authorization") String value);

    @GET("coupons")
    Call<ResponseBody> GetCoupons(
            @Header("Authorization") String value,
            @Query("order_by") String order_by,
            @Query("dir") String dir
    );

    @POST("notifications/register")
    @FormUrlEncoded
    Call<ResponseBody> RegisterDevice(
            @Header("Authorization") String value,
            @FieldMap HashMap<String, String> body);

    @Headers("Cache-Control: max-age=640000")
    @GET("deals/search")
    Call<Deals> SearchDeals(
            @Header("Authorization") String value,
            @Query("key") String key,
            @Query("limit") int limit,
            @Query("offset") int offset
    );

    @POST("subscriptions/add")
    @FormUrlEncoded
    Call<ResponseBody> Subscribe(
            @Header("Authorization") String value,
            @FieldMap HashMap<String, String> body
    );

    @GET("merchants")
    Call<Merchants> GetMerchants(
            @Header("Authorization") String value
    );
}
