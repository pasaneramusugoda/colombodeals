package com.syncbridge.colombodeals.api;

import android.content.Context;

import com.syncbridge.colombodeals.R;
import com.syncbridge.colombodeals.common.Functions;
import com.syncbridge.colombodeals.common.Log;
import com.syncbridge.colombodeals.common.OrderBy;
import com.syncbridge.colombodeals.models.Banners;
import com.syncbridge.colombodeals.models.DealDetail;
import com.syncbridge.colombodeals.models.Deals;
import com.syncbridge.colombodeals.models.Merchants;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Pasan Eramusugoda on 4/15/2016.
 */
public class ApiUsage {
    protected static final String TAG = ApiUsage.class.getSimpleName();
    private static Api mApi;
    private static String mAuthValue;
    private static String BASE_URL;

    public ApiUsage(Context context) {
        mAuthValue = Functions.GetAuthValue(context);
        BASE_URL = Functions.GetStringResource(context, R.string.service_base_url);

        OkHttpClient httpClient = new OkHttpClient.Builder()
                .connectTimeout(180, TimeUnit.SECONDS)
                .readTimeout(180, TimeUnit.SECONDS)
                .writeTimeout(180, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true)
                .build();

        Retrofit mClient = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient)
                .build();

        mApi = mClient.create(Api.class);
    }

    public static void UserAuthentication(String email, String password, final Callback<ResponseBody> responseBodyCallback) {
        mApi.UserLogin(mAuthValue, email, password).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d(TAG, call.request().url().toString());
                responseBodyCallback.onResponse(call, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, call.request().url().toString());
                Log.e(TAG, t.toString());
                responseBodyCallback.onFailure(call, t);
            }
        });
    }

    public static void UserAuthenticationSocial(String name, String email, String uid, String provider, String image, final Callback<ResponseBody> responseBodyCallback) {
        HashMap<String, String> body = new HashMap<>(5);
        body.put("uid", uid);
        body.put("name", name);
        body.put("email", email);
        body.put("image", image);
        body.put("provider", provider);

        mApi.UserSocialLogin(mAuthValue, body).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d(TAG, call.request().url().toString());
                responseBodyCallback.onResponse(call, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, call.request().url().toString());
                Log.e(TAG, t.toString());
                responseBodyCallback.onFailure(call, t);
            }
        });
    }

    public static void UserCreate(String email, String password, String name, final Callback<ResponseBody> responseBodyCallback) {
        HashMap<String, String> body = new HashMap<>(3);
        body.put("name", name);
        body.put("email", email);
        body.put("password", password);

        mApi.UserCreate(mAuthValue, body).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d(TAG, call.request().url().toString());
                responseBodyCallback.onResponse(call, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, call.request().url().toString());
                Log.e(TAG, t.toString());
                responseBodyCallback.onFailure(call, t);
            }
        });
    }

    public static void UserLogout(String customerId, String token, final Callback<ResponseBody> responseBodyCallback) {
        HashMap<String, String> body = new HashMap<>(2);
        body.put("customer_id", customerId);
        body.put("token", token);

        mApi.UserLogout(mAuthValue, body).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d(TAG, call.request().url().toString());
                responseBodyCallback.onResponse(call, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, call.request().url().toString());
                Log.e(TAG, t.toString());
                responseBodyCallback.onFailure(call, t);
            }
        });
    }

    public static void UserUpdate(String customerId, String name, String c_password, String n_password,
                                  String image, String token, final Callback<ResponseBody> responseBodyCallback) {

        HashMap<String, String> body = new HashMap<>(5);
        body.put("id", customerId);
        body.put("name", name);
        body.put("c_password", c_password);
        body.put("n_password", n_password);
        body.put("image", image);
        body.put("token", token);

        mApi.UserUpdate(mAuthValue, body).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d(TAG, call.request().url().toString());
                responseBodyCallback.onResponse(call, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, call.request().url().toString());
                Log.e(TAG, t.toString());
                responseBodyCallback.onFailure(call, t);
            }
        });
    }

    public static void GetDeals(int limit, int offset, final Callback<Deals> responseBodyCallback) {
        mApi.GetDeals(mAuthValue, limit, offset)
                .enqueue(new Callback<Deals>() {
                    @Override
                    public void onResponse(Call<Deals> call, Response<Deals> response) {
                        Log.d(TAG, call.request().url().toString());
                        responseBodyCallback.onResponse(call, response);
                    }

                    @Override
                    public void onFailure(Call<Deals> call, Throwable t) {
                        Log.e(TAG, call.request().url().toString());
                        Log.e(TAG, t.toString());
                        responseBodyCallback.onFailure(call, t);
                    }
                });
    }

    public static void GetDeals(String category, int limit, int offset, final Callback<Deals> responseBodyCallback) {
        mApi.GetDeals(mAuthValue, category, limit, offset).enqueue(new Callback<Deals>() {
            @Override
            public void onResponse(Call<Deals> call, Response<Deals> response) {
                Log.d(TAG, call.request().url().toString());
                responseBodyCallback.onResponse(call, response);
            }

            @Override
            public void onFailure(Call<Deals> call, Throwable t) {
                Log.e(TAG, call.request().url().toString());
                Log.e(TAG, t.toString());
                responseBodyCallback.onFailure(call, t);
            }
        });
    }

    public static void GetDeals(String merchants, String startDate, String endDate, final Callback<Deals> responseBodyCallback) {
        HttpUrl _url;
        String url = null;

        if(merchants != null && !merchants.isEmpty()) {
            url = merchants;
        }

        if(startDate != null) {
            if(url != null)
                url = url + "&start_dt=" + startDate;
            else
                url = "start_dt=" + startDate;
        }

        if(endDate != null) {
            if(url != null)
                url = url + "&expiry_dt=" + endDate;
            else
                url = "expiry_dt=" + endDate;
        }

        _url = HttpUrl.parse(String.format("%sdeals/merchants", BASE_URL) + (url == null ? "" : "?"+url));

        mApi.GetDeals(mAuthValue, _url).enqueue(new Callback<Deals>() {
            @Override
            public void onResponse(Call<Deals> call, Response<Deals> response) {
                Log.d(TAG, call.request().url().toString());
                responseBodyCallback.onResponse(call, response);
            }

            @Override
            public void onFailure(Call<Deals> call, Throwable t) {
                Log.e(TAG, call.request().url().toString());
                Log.e(TAG, t.toString());
                responseBodyCallback.onFailure(call, t);
            }
        });
    }

    public static void GetDealsNearby(double lat, double lon, int radius, final Callback<Deals> responseBodyCallback) {
        mApi.GetDealsNearby(mAuthValue, lat, lon, radius).enqueue(new Callback<Deals>() {
            @Override
            public void onResponse(Call<Deals> call, Response<Deals> response) {
                Log.d(TAG, call.request().url().toString());
                responseBodyCallback.onResponse(call, response);
            }

            @Override
            public void onFailure(Call<Deals> call, Throwable t) {
                Log.e(TAG, call.request().url().toString());
                Log.e(TAG, t.toString());
                responseBodyCallback.onFailure(call, t);
            }
        });
    }

    public static void GetBanners(final Callback<Banners> responseBodyCallback) {
        mApi.GetBanners(mAuthValue).enqueue(new Callback<Banners>() {
            @Override
            public void onResponse(Call<Banners> call, Response<Banners> response) {
                Log.d(TAG, call.request().url().toString());
                responseBodyCallback.onResponse(call, response);
            }

            @Override
            public void onFailure(Call<Banners> call, Throwable t) {
                Log.e(TAG, call.request().url().toString());
                Log.e(TAG, t.toString());
                responseBodyCallback.onFailure(call, t);
            }
        });
    }

    public static void GetDeal(String dealId, final Callback<DealDetail> responseBodyCallback) {
        mApi.GetDeal(mAuthValue, dealId).enqueue(new Callback<DealDetail>() {
            @Override
            public void onResponse(Call<DealDetail> call, Response<DealDetail> response) {
                Log.d(TAG, call.request().url().toString());
                responseBodyCallback.onResponse(call, response);
            }

            @Override
            public void onFailure(Call<DealDetail> call, Throwable t) {
                Log.e(TAG, call.request().url().toString());
                Log.e(TAG, t.toString());
                responseBodyCallback.onFailure(call, t);
            }
        });
    }

    public static void AddReview(String dealId, String review, int rating, int userId, String nickName, String token, final Callback<ResponseBody> responseBodyCallback) {
        HashMap<String, String> body = new HashMap<>(6);
        body.put("deal_id", dealId);
        body.put("review", review);
        body.put("rating", String.valueOf(rating));
        body.put("user_id", String.valueOf(userId));
        body.put("author_nick_name", nickName);
        body.put("token", token);

        mApi.AddReview(mAuthValue, body).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d(TAG, call.request().url().toString());
                responseBodyCallback.onResponse(call, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, call.request().url().toString());
                Log.e(TAG, t.toString());
                responseBodyCallback.onFailure(call, t);
            }
        });
    }

    public static void GetCustomer(String customerId, String token, final Callback<ResponseBody> responseBodyCallback) {
        mApi.GetCustomer(mAuthValue, customerId, token).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d(TAG, call.request().url().toString());
                responseBodyCallback.onResponse(call, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, call.request().url().toString());
                Log.e(TAG, t.toString());
                responseBodyCallback.onFailure(call, t);
            }
        });
    }

    public static void GetCoupons(final Callback<ResponseBody> responseBodyCallback) {
        mApi.GetCoupons(mAuthValue).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d(TAG, call.request().url().toString());
                responseBodyCallback.onResponse(call, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, call.request().url().toString());
                Log.e(TAG, t.toString());
                responseBodyCallback.onFailure(call, t);
            }
        });
    }

    public static void GetCoupons(OrderBy orderBy, String dir, final Callback<ResponseBody> responseBodyCallback) {
        mApi.GetCoupons(mAuthValue, orderBy.name(), dir).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d(TAG, call.request().url().toString());
                responseBodyCallback.onResponse(call, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, call.request().url().toString());
                Log.e(TAG, t.toString());
                responseBodyCallback.onFailure(call, t);
            }
        });
    }

    public static void RegisterDevice(String customerId, String deviceId, final Callback<ResponseBody> responseBodyCallback) {
        HashMap<String, String> body = new HashMap<>(5);
        body.put("customer_id", customerId);
        body.put("register_id", deviceId);

        mApi.RegisterDevice(mAuthValue, body).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d(TAG, call.request().url().toString());
                responseBodyCallback.onResponse(call, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, call.request().url().toString());
                Log.e(TAG, t.toString());
                responseBodyCallback.onFailure(call, t);
            }
        });
    }

    public static void SearchDeals(String key, int limit, int offset, final Callback<Deals> responseBodyCallback) {
        mApi.SearchDeals(mAuthValue, key, limit, offset).enqueue(new Callback<Deals>() {
            @Override
            public void onResponse(Call<Deals> call, Response<Deals> response) {
                Log.d(TAG, call.request().url().toString());
                responseBodyCallback.onResponse(call, response);
            }

            @Override
            public void onFailure(Call<Deals> call, Throwable t) {
                Log.e(TAG, call.request().url().toString());
                Log.e(TAG, t.toString());
                responseBodyCallback.onFailure(call, t);
            }
        });
    }

    public static void Subscribe(String email, final Callback<ResponseBody> responseBodyCallback) {
        HashMap<String, String> body = new HashMap<>(1);
        body.put("email", email);

        mApi.Subscribe(mAuthValue, body).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d(TAG, "Subscribe", call.request().url().toString());
                responseBodyCallback.onResponse(call, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "Subscribe", call.request().url().toString());
                Log.e(TAG, "Subscribe", t.toString());
                responseBodyCallback.onFailure(call, t);
            }
        });
    }

    public static void GetMerchants(final Callback<Merchants> responseBodyCallback) {
        mApi.GetMerchants(mAuthValue).enqueue(new Callback<Merchants>() {
            @Override
            public void onResponse(Call<Merchants> call, Response<Merchants> response) {
                Log.d(TAG, call.request().url().toString());
                responseBodyCallback.onResponse(call, response);
            }

            @Override
            public void onFailure(Call<Merchants> call, Throwable t) {
                Log.e(TAG, call.request().url().toString());
                Log.e(TAG, t.toString());
                responseBodyCallback.onFailure(call, t);
            }
        });
    }
}
