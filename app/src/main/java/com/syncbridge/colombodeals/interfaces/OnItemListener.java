package com.syncbridge.colombodeals.interfaces;

import android.view.View;

import com.syncbridge.colombodeals.common.Identity;

/**
 * Created by Pasan Eramusugoda on 4/8/2016.
 */
public interface OnItemListener {
    void onItemListener(View v);

    void onItemListener(View v, int region);

    void onItemListener(int position, Identity identity, Object object);
}
