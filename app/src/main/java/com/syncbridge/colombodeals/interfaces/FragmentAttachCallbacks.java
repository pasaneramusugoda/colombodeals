package com.syncbridge.colombodeals.interfaces;

/**
 * Created by Pasan Eramusugoda on 19/2/2016.
 */
public interface FragmentAttachCallbacks {
    void OnFragmentAttachListener(int fragment_id, String title);
}
