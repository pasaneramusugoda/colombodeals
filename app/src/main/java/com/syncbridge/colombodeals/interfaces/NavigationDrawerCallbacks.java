package com.syncbridge.colombodeals.interfaces;

/**
 * Created by Pasan Eramusugoda on 4/8/2016.
 */
public interface NavigationDrawerCallbacks {
    void onNavigationDrawerItemSelected(int position);
}
