package com.syncbridge.colombodeals.views.activities;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.VisibleRegion;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.syncbridge.colombodeals.R;
import com.syncbridge.colombodeals.adapters.CardDetailAdapter;
import com.syncbridge.colombodeals.adapters.CommentAdapter;
import com.syncbridge.colombodeals.api.ApiUsage;
import com.syncbridge.colombodeals.common.Functions;
import com.syncbridge.colombodeals.common.Log;
import com.syncbridge.colombodeals.common.ResponseErrorMessageHandler;
import com.syncbridge.colombodeals.models.Deal;
import com.syncbridge.colombodeals.models.DealDetail;
import com.syncbridge.colombodeals.models.Deals;
import com.syncbridge.colombodeals.models.MarkerObject;
import com.syncbridge.colombodeals.models.PaymentCard;
import com.syncbridge.colombodeals.models.Review;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import io.techery.properratingbar.ProperRatingBar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, MaterialDialog.SingleButtonCallback,
        LocationListener, GoogleMap.OnCameraChangeListener, GoogleMap.OnMarkerClickListener, GoogleMap.OnMapClickListener, GoogleMap.OnInfoWindowClickListener {

    protected static final String TAG = MapsActivity.class.getSimpleName();
    private static final int PERMISSIONS_REQUEST_MAPS_RECEIVE = 533;
    private static final double EARTH_RADIUS = 3958.75; // Earth radius;
    private static final int METER_CONVERSION = 1609;
    private Context mContext;
    private ResponseErrorMessageHandler mREM;
    private List<Deal> mDeals = new ArrayList<>();
    private List<MarkerObject> mMarkers = new ArrayList<>();
    private Marker mCenterMarker;
    private LatLng mLatLng;
    private Timer mTimer;
    private int mRadius = 5; //in km
    private boolean mIsOnMarker = false;

    private GoogleMap mMap;

    //region deal varis
    private SlidingUpPanelLayout mLayout;
    private View mDragView;
    private List<PaymentCard> mCards = new ArrayList<>();
    private List<Review> mComments = new ArrayList<>();
    private CardDetailAdapter mCardDetailAdapter;
    private CommentAdapter mCommentAdapter;

    private Deal mDeal;

    private View mBaseView;
    private AppCompatTextView mTitle;
    private AppCompatTextView mCategory;
    private AppCompatTextView mHighlightSection;
    private AppCompatTextView mHighlight;
    private ProperRatingBar mRatingBar;
    private AppCompatImageView mCategoryImage;
    private View mAddressBase;
    private AppCompatTextView mAddress;
    private View mPhoneBase;
    private AppCompatTextView mPhone;
    private View mWebBase;
    private AppCompatTextView mWeb;
    private AppCompatTextView mCommentSection;
    //endregion

    //region functions

    //region map
    protected Marker createMarker(double latitude, double longitude, String title, String snippet, String category) {

        int iconResID;

        switch (category.toLowerCase()) {
            case "fashion":
                iconResID = R.drawable.fashion_blue;
                break;
            case "travel":
                iconResID = R.drawable.travel_blue;
                break;
            case "leisure":
                iconResID = R.drawable.leisure_blue;
                break;
            case "restaurant":
                iconResID = R.drawable.restaurant_blue;
                break;
            default:
                iconResID = R.drawable.location_blue;
                break;
        }

        return mMap.addMarker(new MarkerOptions()
                .position(new LatLng(latitude, longitude))
                .title(title)
                .snippet(snippet)
                .icon(
                        BitmapDescriptorFactory.fromBitmap(
                                Bitmap.createScaledBitmap(
                                        BitmapFactory.decodeResource(mContext.getResources(), iconResID), 48, 48, false))));
    }

    private void updateMap() {
        mMarkers.clear();

        for (Deal deal : mDeals) {
            if (mMap != null) {
                Marker marker = createMarker(deal.getLat(), deal.getLon(), deal.getMerchant(), deal.getTitle(), deal.getCategory());
                mMarkers.add(new MarkerObject(marker, deal.getId()));
                Log.d(TAG, String.valueOf(marker.hashCode()));
            }
        }
    }

    private void getDeals() {

        if (!Functions.IsInternetOn(mContext)) {
            mREM.showRetry(Functions.GetStringResource(mContext, R.string.error_internet_not_available), -1, null, null);
            return;
        }

        if (mLatLng == null)
            return;

        ApiUsage.GetDealsNearby(mLatLng.latitude, mLatLng.longitude, mRadius, new Callback<Deals>() {
            @Override
            public void onResponse(Call<Deals> call, Response<Deals> response) {
                if (response.isSuccessful()) {
                    switch (response.body().getStatus()) {
                        case 1:
                            mDeals.clear();
                            mDeals.addAll(response.body().getDeals());
                            updateMap();
                            break;
                        default:
                            Log.e(TAG, response.body().getMsg());
                            updateMap();
                            break;
                    }
                } else {
                    mREM.showRetry(response.body().getMsg(), response.code(), null, null);
                }
            }

            @Override
            public void onFailure(Call<Deals> call, Throwable t) {
                Log.e(TAG, t.toString());
                mREM.showRetry(null, -1, t, null);
            }
        });
    }

    /**
     * Enables the My Location layer if the fine location permission has been granted.
     */
    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
            ActivityCompat.requestPermissions(MapsActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_MAPS_RECEIVE);
        } else if (mMap != null) {
            // Access to the location has been granted to the app.
            mMap.setMyLocationEnabled(true);

            // Getting LocationManager object from System Service LOCATION_SERVICE
            LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

            // Creating a criteria object to retrieve provider
            Criteria criteria = new Criteria();

            // Getting the name of the best provider
            String provider = locationManager.getBestProvider(criteria, true);

            // Getting Current Location
            Location location = locationManager.getLastKnownLocation(provider);

            if (location != null) {
                // Getting latitude of the current location
                double latitude = location.getLatitude();

                // Getting longitude of the current location
                double longitude = location.getLongitude();

                mLatLng = new LatLng(latitude, longitude);

                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mLatLng, 13));
                mCenterMarker = createMarker(mLatLng.latitude, mLatLng.longitude, "Your current location", "", "none");
            }

            getDeals();
        }
    }

    public void animateMarker(final Marker marker, final LatLng toPosition,
                              final boolean hideMarker) {
        if (mMap == null || marker == null)
            return;

        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection proj = mMap.getProjection();
        Point startPoint = proj.toScreenLocation(marker.getPosition());
        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
        final long duration = 500;
        final Interpolator interpolator = new LinearInterpolator();
        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed
                        / duration);
                double lng = t * toPosition.longitude + (1 - t)
                        * startLatLng.longitude;
                double lat = t * toPosition.latitude + (1 - t)
                        * startLatLng.latitude;
                marker.setPosition(new LatLng(lat, lng));
                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                } else {
                    if (hideMarker) {
                        marker.setVisible(false);
                    } else {
                        marker.setVisible(true);
                    }
                }
            }
        });
    }

    public double distanceFrom(double lat1, double lng1, double lat2, double lng2) {
        // Return distance between 2 points, stored as 2 pair location;
        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(Math.toRadians(lat1))
                * Math.cos(Math.toRadians(lat2)) * Math.sin(dLng / 2) * Math.sin(dLng / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double dist = EARTH_RADIUS * c;
        return Double.valueOf(dist * METER_CONVERSION).floatValue();
    }
    //endregion

    //region deal

    private void getDeal(String dealId) {

        if (dealId == null || dealId.isEmpty()) {
            new MaterialDialog.Builder(mContext)
                    .title(Functions.GetStringResource(mContext, R.string.dialog_title_error_response))
                    .content("Sorry unable to gather data.\nPlease restart the application.")
                    .positiveText(R.string.dialog_button_ok)
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            dialog.dismiss();
                        }
                    })
                    .show();
            return;
        }

        ApiUsage.GetDeal(dealId, new Callback<DealDetail>() {
            @Override
            public void onResponse(Call<DealDetail> call, Response<DealDetail> response) {
                if (response.isSuccessful()) {
                    switch (response.body().getStatus()) {
                        case 1:
                            fetchData(response.body().getDeal());
                            break;
                        default:
                            Log.e(TAG, response.body().getMsg());
                            break;
                    }
                } else {
                    Toast.makeText(mContext, response.body().getMsg(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<DealDetail> call, Throwable t) {
                Log.e(TAG, t.toString());
                Toast.makeText(mContext, t.toString(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void fetchData(Deal deal) {
        mDeal = deal;
        mTitle.setText(mDeal.getTitle());
        mCategory.setText(mDeal.getCategory());
        mHighlightSection.setText("Highlights");
        mHighlight.setText(mDeal.getHighlights());
        mRatingBar.setRating(mDeal.getRating().intValue());
        mCommentSection.setText("Comments");

        switch (mDeal.getCategory().toLowerCase()) {
            case "restaurant":
                mCategoryImage.setImageDrawable(Functions.GetDrawable(mContext, R.drawable.restaurant_blue));
                break;
            case "leisure":
                mCategoryImage.setImageDrawable(Functions.GetDrawable(mContext, R.drawable.leisure_blue));
                break;
            case "travel":
                mCategoryImage.setImageDrawable(Functions.GetDrawable(mContext, R.drawable.travel_blue));
                break;
            case "fashion":
                mCategoryImage.setImageDrawable(Functions.GetDrawable(mContext, R.drawable.fashion_blue));
                break;
            default:
                mCategoryImage.setVisibility(View.GONE);
                break;
        }

        if (deal.getLocation() != null && !deal.getLocation().isEmpty()) {
            mAddress.setText(deal.getLocation());
        } else {
            mAddressBase.setVisibility(View.GONE);
        }

        if (deal.getPhone() != null && !deal.getPhone().isEmpty()) {
            mPhone.setText(deal.getPhone());
        } else {
            mPhoneBase.setVisibility(View.GONE);
        }

        if (deal.getWeb() != null && !deal.getWeb().isEmpty()) {
            mWeb.setText(deal.getWeb());
        } else {
            mWebBase.setVisibility(View.GONE);
        }

        mCards.clear();
        mCards.addAll(mDeal.getPaymentCards());
        mCardDetailAdapter.notifyDataSetChanged();

        mComments.clear();
        mComments.addAll(mDeal.getReviews());
        mCommentAdapter.notifyDataSetChanged();

        if (mComments.size() == 0)
            mCommentSection.setVisibility(View.GONE);

        mBaseView.setVisibility(View.VISIBLE);
        mDragView.setVisibility(View.VISIBLE);
        mLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
    }

    //endregion
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        mContext = MapsActivity.this;

        mREM = new ResponseErrorMessageHandler(mContext, this);
        mREM.cancelable(false);
        mREM.showCancelButton(true);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //region deal
        mLayout = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);
        mLayout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
                Log.d(TAG, "onPanelSlide, offset " + slideOffset);
            }

            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
                Log.d(TAG, "onPanelStateChanged " + newState);
            }
        });
        mLayout.setFadeOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                mLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
            }
        });

        mDragView = findViewById(R.id.drag_view);
        if (mDragView != null)
            mDragView.setVisibility(View.INVISIBLE);

        mBaseView = findViewById(R.id.base_view);
        if (mBaseView != null)
            mBaseView.setVisibility(View.INVISIBLE);

        mTitle = (AppCompatTextView) findViewById(R.id.tv_title);
        mCategory = (AppCompatTextView) findViewById(R.id.tv_category);
        mHighlightSection = (AppCompatTextView) findViewById(R.id.tv_highlight_section);
        mHighlight = (AppCompatTextView) findViewById(R.id.tv_highlight);
        mRatingBar = (ProperRatingBar) findViewById(R.id.rating);
        mAddress = (AppCompatTextView) findViewById(R.id.address);
        mAddressBase = findViewById(R.id.address_base);
        mPhone = (AppCompatTextView) findViewById(R.id.phone);
        mPhoneBase = findViewById(R.id.phone_base);
        mWeb = (AppCompatTextView) findViewById(R.id.web);
        mWebBase = findViewById(R.id.web_base);
        mCategoryImage = (AppCompatImageView) findViewById(R.id.image_category);
        mCommentSection = (AppCompatTextView) findViewById(R.id.tv_comments_section);

        mCardDetailAdapter = new CardDetailAdapter(mContext, mCards);

        RecyclerView mCardsRecyclerView = (RecyclerView) findViewById(R.id.rv_cards);
        if (mCardsRecyclerView != null) {
            mCardsRecyclerView.setHasFixedSize(true);
            mCardsRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
            mCardsRecyclerView.setAdapter(mCardDetailAdapter);
        }

        mCommentAdapter = new CommentAdapter(mContext, mComments);

        RecyclerView mCommentsRecyclerView = (RecyclerView) findViewById(R.id.rv_comments);
        if (mCommentsRecyclerView != null) {
            mCommentsRecyclerView.setHasFixedSize(true);
            mCommentsRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
            mCommentsRecyclerView.setAdapter(mCommentAdapter);
        }
        //endregion
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnCameraChangeListener(this);
        mMap.setOnMarkerClickListener(this);
        mMap.setOnInfoWindowClickListener(this);
        mMap.setOnMapClickListener(this);
        enableMyLocation();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_MAPS_RECEIVE) {
            if (permissions.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                enableMyLocation();
            } else {
                Toast.makeText(mContext, "Permission denied to get your current location.", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
        switch (which) {
            case NEGATIVE:
                finish();
                dialog.dismiss();
                break;
            case POSITIVE:
                dialog.dismiss();
                getDeals();
                break;
        }
    }

    @Override
    public void onCameraChange(final CameraPosition cameraPosition) {
        if (mIsOnMarker)
            return;

        mLatLng = cameraPosition.target;
        animateMarker(mCenterMarker, mLatLng, false);

        Log.d(TAG, "onCameraChange", mLatLng.toString());

        VisibleRegion visibleRegion = mMap.getProjection().getVisibleRegion();
        LatLng nearLeft = visibleRegion.nearLeft;
        LatLng nearRight = visibleRegion.nearRight;
        LatLng farLeft = visibleRegion.farLeft;
        LatLng farRight = visibleRegion.farRight;
        double dist_w = distanceFrom(nearLeft.latitude, nearLeft.longitude, nearRight.latitude, nearRight.longitude);
        double dist_h = distanceFrom(farLeft.latitude, farLeft.longitude, farRight.latitude, farRight.longitude);
        Log.d(TAG, "DISTANCE", "DISTANCE WIDTH: " + dist_w + " DISTANCE HEIGHT: " + dist_h);

        Long radius = Math.round(Math.max(dist_w, dist_h) / 2);
        mRadius = Math.round(radius.intValue() / 1000); //into km

        Log.d(TAG, "radius", String.valueOf(radius));
        Log.d(TAG, "mRadius", String.valueOf(mRadius));

        if (mTimer != null) {
            mTimer.purge();
            mTimer.cancel();
        }
        mTimer = new Timer();
        mTimer.schedule(new TimerTask() {
            public void run() {
                getDeals();
                mTimer.cancel(); // also just top the timer thread, otherwise, you may receive a crash report
            }
        }, 2000);
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "onLocationChanged", location.toString());
    }

    @Override
    public void onBackPressed() {
        if (mLayout != null &&
                (mLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED || mLayout.getPanelState() == SlidingUpPanelLayout.PanelState.ANCHORED)) {
            mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            mLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onMarkerClick(final Marker marker) {
        mIsOnMarker = true;
        return false;
    }

    @Override
    public void onMapClick(LatLng latLng) {
        mIsOnMarker = false;
        mLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        Log.d(TAG, "onInfoWindowClick", "true");
        Log.d(TAG, "hashCode", String.valueOf(marker.hashCode()));
        mIsOnMarker = true;
        if (mLayout != null) {
            MarkerObject object = null;

            for (MarkerObject _object : mMarkers) {
                Marker _marker = _object.getMarker();

                Log.d(TAG, "lat", String.valueOf(_marker.getPosition().latitude + "-" + marker.getPosition().latitude));
                Log.d(TAG, "lon", String.valueOf(_marker.getPosition().longitude + "-" + marker.getPosition().longitude));

                if (_marker.getPosition().latitude == marker.getPosition().latitude
                        && _marker.getPosition().longitude == marker.getPosition().longitude
//                        && _marker.getTitle().trim() == marker.getTitle().trim()
//                        && _marker.getSnippet().trim() == marker.getSnippet().trim()
                        ) {
                    object = _object;
                    break;
                }
            }

            if (object != null) {
                if (mDeal == null || !mDeal.getId().equals(object.getDealId())) {
                    getDeal(object.getDealId());
                } else {
                    mLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                }
            } else {
                Log.e(TAG, "not found");
            }
        }
    }
}
