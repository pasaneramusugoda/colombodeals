package com.syncbridge.colombodeals.views.activities;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.raizlabs.android.dbflow.sql.language.NameAlias;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.sql.language.Select;
import com.syncbridge.colombodeals.R;
import com.syncbridge.colombodeals.adapters.NotificationAdapter;
import com.syncbridge.colombodeals.common.Identity;
import com.syncbridge.colombodeals.common.Preference;
import com.syncbridge.colombodeals.controllers.AppController;
import com.syncbridge.colombodeals.events.NotificationEvent;
import com.syncbridge.colombodeals.interfaces.OnItemListener;
import com.syncbridge.colombodeals.models.Notification;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

public class NotificationActivity extends AppCompatActivity implements OnItemListener {

    Context mContext;
    private RecyclerView mRecyclerView;
    private NotificationAdapter mAdapter;

    private Handler mUiHandler = new Handler();

    private List<Notification> mNotifications = new ArrayList<>();

    //region imp

    private void clearAll() {
        SQLite.delete(Notification.class).execute();
        mNotifications.clear();
        mAdapter.notifyDataSetChanged();
    }

    private void loadData() {
        mNotifications.addAll(new Select().from(Notification.class).orderBy(new NameAlias.Builder("id").build(), false).queryList());
    }

    private void updateUnread() {
        for (int i = 0; i < mNotifications.size(); i++) {
            Notification item = mNotifications.get(i);
            if (item != null && item.getIsRead() == 0) {
                item.setIsRead(1);
                item.save();
            }
        }
        mAdapter.notifyDataSetChanged();
        NotificationManager mNotificationManager =
                (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.cancelAll();
    }

    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        mContext = this;

        loadData();

        mRecyclerView = (RecyclerView) findViewById(R.id.rv_notifications);
        assert mRecyclerView != null;
        mRecyclerView.setHasFixedSize(true);

        mAdapter = new NotificationAdapter(mContext, mNotifications, this);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        mUiHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                updateUnread();
            }
        }, 1000);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_notification, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_clear) {
            clearAll();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemListener(View v) {

    }

    @Override
    public void onItemListener(View v, int region) {

    }

    @Override
    public void onItemListener(int position, Identity identity, Object object) {
        if (identity == Identity.NOTIFICATION) {
            Intent intent = new Intent(mContext, DealDetailActivity.class);
            intent.putExtra(Preference.Keys.DEAL_ID.name(), String.valueOf(((Notification) object).getDealId()));
            startActivity(intent);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppController.activityPaused();
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.activityResumed();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onEvent(NotificationEvent event) {
        mNotifications.add(0, event.notification);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mAdapter != null) {
                    mAdapter.notifyDataSetChanged();
                    mRecyclerView.smoothScrollToPosition(0);
                }
            }
        });
    }
}
