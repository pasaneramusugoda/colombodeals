package com.syncbridge.colombodeals.views.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.syncbridge.colombodeals.R;
import com.syncbridge.colombodeals.api.ApiUsage;
import com.syncbridge.colombodeals.common.Functions;
import com.syncbridge.colombodeals.common.Identity;
import com.syncbridge.colombodeals.common.Log;
import com.syncbridge.colombodeals.common.LoginType;
import com.syncbridge.colombodeals.common.Preference;
import com.syncbridge.colombodeals.common.ProgressBar;
import com.syncbridge.colombodeals.common.ResponseErrorMessageHandler;
import com.syncbridge.colombodeals.interfaces.OnItemListener;
import com.syncbridge.colombodeals.models.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements MaterialDialog.SingleButtonCallback,
        GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {

    private static final String TAG = LoginActivity.class.getSimpleName();
    private static final int RC_SIGN_IN_GOOGLE = 9001;
    private static final int RC_SIGN_IN_NORMAL = 9002;
    private Context mContext;
    private ProgressBar mProgressBar;
    private ResponseErrorMessageHandler mREM;
    private CallbackManager mCallbackManager;
    private GoogleApiClient mGoogleApiClient;
    private MaterialEditText mUsername, mPassword;
    private LoginButton mLoginButtonFB;

    //region functions
    private FacebookCallback<LoginResult> mCallBack = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            GraphRequest request = GraphRequest.newMeRequest(
                    loginResult.getAccessToken(),
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(
                                JSONObject object,
                                GraphResponse response) {

                            Log.d("response: ", response + "");
                            try {
                                User user = new User();
                                user.user_id = object.getString("id");
                                user.email = object.getString("email");
                                user.name = object.getString("name");
                                user.gender = object.getString("gender");
                                user.login_type = LoginType.FB;
                                user.profile_pic_url = object.getJSONObject("picture").getJSONObject("data").getString("url");
                                mProgressBar.stop();
                                authenticateSocial(user);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });

            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,name,email,gender, picture.type(large)");
            request.setParameters(parameters);
            request.executeAsync();
        }

        @Override
        public void onCancel() {
            mProgressBar.stop();
        }

        @Override
        public void onError(FacebookException e) {
            mProgressBar.stop();
            Log.e(TAG, e.toString());
            Toast.makeText(mContext, e.toString(), Toast.LENGTH_LONG).show();
        }
    };

    private void authenticate(final String username, String password, @Nullable final String name) {

        if (!Functions.IsInternetOn(mContext)) {
            mREM.showRetry(Functions.GetStringResource(mContext, R.string.error_internet_not_available), -1, null, null);
            return;
        }

        mProgressBar.start();

        ApiUsage.UserAuthentication(username, password, new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String jsonString = response.body().string();
                    Log.d(TAG, jsonString);

                    JSONObject mObject = new JSONObject(jsonString);

                    switch (mObject.getInt("status")) {
                        case 0:
                            Toast.makeText(mContext, mObject.getString("msg"), Toast.LENGTH_LONG).show();
                            break;
                        case 1:
                            String userId = mObject.getString("customer_id");
                            String token = mObject.getString("token");
                            String tokenExpireAt = mObject.getString("token_expire_at");

                            if (token != null && !token.isEmpty() && userId != null && !userId.isEmpty()) {
                                User user = new User();
                                user.name = name;
                                user.email = username;
                                user.customer_id = userId;
                                user.token = token;
                                user.token_expire_at = tokenExpireAt;
                                user.login_type = LoginType.NORMAL;
                                Preference.getInstance(mContext).setCurrentUser(user);

                                Intent intent = new Intent(mContext, MainActivity.class);
                                startActivity(intent);
                                finish();
                            } else {
                                Toast.makeText(mContext, "Invalid user id or token", Toast.LENGTH_LONG).show();
                            }
                            break;
                    }

                } catch (IOException | JSONException e) {
                    Log.e(TAG, e.toString());
                    Toast.makeText(mContext, e.toString(), Toast.LENGTH_LONG).show();
                }

                mProgressBar.stop();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                mProgressBar.stop();
                Log.e(TAG, "authenticate", t.toString());
                Toast.makeText(mContext, t.toString(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void authenticateSocial(@NonNull final User mUser) {

        if (!Functions.IsInternetOn(mContext)) {
            mREM.showRetry(Functions.GetStringResource(mContext, R.string.error_internet_not_available), -1, null, null);
            return;
        }

        mProgressBar.start();

        String provider = "";

        switch (mUser.login_type) {
            case FB:
                provider = "facebook";
                break;
            case GOOGLE:
                provider = "google";
                break;
        }

        ApiUsage.UserAuthenticationSocial(mUser.name, mUser.email, mUser.user_id, provider, mUser.profile_pic_url, new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String jsonString = response.body().string();
                    Log.d(TAG, jsonString);

                    JSONObject mObject = new JSONObject(jsonString);

                    switch (mObject.getInt("status")) {
                        case 0:
                            Toast.makeText(mContext, mObject.getString("msg"), Toast.LENGTH_LONG).show();
                            break;
                        case 1:
                            String userId = mObject.getString("customer_id");
                            String token = mObject.getString("token");
                            String tokenExpireAt = mObject.getString("token_expire_at");

                            if (token != null && !token.isEmpty() && userId != null && !userId.isEmpty()) {
                                mUser.customer_id = userId;
                                mUser.token = token;
                                mUser.token_expire_at = tokenExpireAt;
                                Preference.getInstance(mContext).setCurrentUser(mUser);

                                Intent intent = new Intent(mContext, MainActivity.class);
                                startActivity(intent);
                                finish();
                            } else {
                                Toast.makeText(mContext, "Invalid user id or token", Toast.LENGTH_LONG).show();
                            }
                            break;
                    }

                } catch (IOException | JSONException e) {
                    Log.e(TAG, e.toString());
                    Toast.makeText(mContext, e.toString(), Toast.LENGTH_LONG).show();
                }

                mProgressBar.stop();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                mProgressBar.stop();
                Log.e(TAG, "authenticateSocial", t.toString());
                Toast.makeText(mContext, t.toString(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void signIn() {
        if(!mGoogleApiClient.isConnected()) {
            Toast.makeText(mContext, "Google client still not connected. Please try again", Toast.LENGTH_LONG).show();
            return;
        }

        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN_GOOGLE);
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            if (acct != null) {
                User user = new User();
                user.user_id = acct.getId();
                user.name = acct.getDisplayName();
                user.email = acct.getEmail();
                user.profile_pic_url = acct.getPhotoUrl() == null ? "" : acct.getPhotoUrl().toString();
                user.token = acct.getIdToken();
                user.login_type = LoginType.GOOGLE;

                authenticateSocial(user);
            } else {
                Toast.makeText(mContext, "User information not received. Please try again.", Toast.LENGTH_LONG).show();
            }
        } else {
            // Signed out, show unauthenticated UI.
            Log.e(TAG, "handleSignInResult", result.toString());
            Log.d(TAG, "handleSignInResult", String.valueOf(result.getStatus().getStatusCode()) + " : " + result.getStatus().getStatusMessage());

            Toast.makeText(mContext, "Unauthorized user", Toast.LENGTH_LONG).show();
        }
    }

    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mContext = LoginActivity.this;

        mProgressBar = ProgressBar.getInstance(findViewById(R.id.progress_container));
        mREM = new ResponseErrorMessageHandler(mContext, this);
        mREM.cancelable(false);
        mREM.showCancelButton(true);

        //region fb
        mCallbackManager = CallbackManager.Factory.create();
        mLoginButtonFB = (LoginButton) findViewById(R.id.login_button);
        if (mLoginButtonFB != null) {
            mLoginButtonFB.setReadPermissions("public_profile", "email", "user_friends");
        }
        //endregion

        //region google

        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        // Build a GoogleApiClient with access to the Google Sign-In API and the
        // options specified by gso.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        //endregion

        mUsername = (MaterialEditText) findViewById(R.id.username);
        mPassword = (MaterialEditText) findViewById(R.id.password);

        mUsername.addValidator(Functions.ValidationRequired(mContext));
        mUsername.addValidator(Functions.ValidationEmail(mContext));
        mPassword.addValidator(Functions.ValidationRequired(mContext));

        findViewById(R.id.sign_in).setOnClickListener(this);
        findViewById(R.id.sign_up).setOnClickListener(this);
        findViewById(R.id.forgot).setOnClickListener(this);
        findViewById(R.id.facebook).setOnClickListener(this);
        findViewById(R.id.google).setOnClickListener(this);
    }

    @Override
    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
        switch (which) {
            case NEGATIVE:
                dialog.dismiss();
                break;
            case POSITIVE:
                dialog.dismiss();
                authenticate(mUsername.getText().toString(), mPassword.getText().toString(), null);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN_NORMAL) {
            if (resultCode == RESULT_OK) {
                String name = data.getStringExtra("name");
                String email = data.getStringExtra("email");
                String password = data.getStringExtra("password");

                authenticate(email, password, name);
            }
        } else if (requestCode == RC_SIGN_IN_GOOGLE) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        } else {
            mCallbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(TAG, connectionResult.toString());
        Toast.makeText(mContext, connectionResult.getErrorMessage(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.sign_in:
                if (mUsername.validate()
                        && mPassword.validate()) {
                    authenticate(mUsername.getText().toString(), mPassword.getText().toString(), null);
                }
                break;
            case R.id.sign_up:
                Intent intent = new Intent(mContext, SignupActivity.class);
                startActivityForResult(intent, RC_SIGN_IN_NORMAL);
                break;
            case R.id.facebook:

                if (!Functions.IsInternetOn(mContext)) {
                    mREM.showRetry(Functions.GetStringResource(mContext, R.string.error_internet_not_available), -1, null, null);
                    return;
                }

                mProgressBar.start();
                mLoginButtonFB.performClick();
                mLoginButtonFB.setPressed(true);
                mLoginButtonFB.invalidate();
                mLoginButtonFB.registerCallback(mCallbackManager, mCallBack);
                mLoginButtonFB.setPressed(false);
                mLoginButtonFB.invalidate();
                break;
            case R.id.google:
                if (!Functions.IsInternetOn(mContext)) {
                    mREM.showRetry(Functions.GetStringResource(mContext, R.string.error_internet_not_available), -1, null, null);
                    return;
                }
                signIn();
                break;
        }
    }
}
