package com.syncbridge.colombodeals.views.activities;

import android.content.Context;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.syncbridge.colombodeals.R;
import com.syncbridge.colombodeals.adapters.CardDetailAdapter;
import com.syncbridge.colombodeals.adapters.CommentAdapter;
import com.syncbridge.colombodeals.api.ApiUsage;
import com.syncbridge.colombodeals.common.Functions;
import com.syncbridge.colombodeals.common.Identity;
import com.syncbridge.colombodeals.common.Log;
import com.syncbridge.colombodeals.common.Preference;
import com.syncbridge.colombodeals.common.ProgressBar;
import com.syncbridge.colombodeals.common.ResponseErrorMessageHandler;
import com.syncbridge.colombodeals.common.SaveInstanceStates;
import com.syncbridge.colombodeals.interfaces.OnItemListener;
import com.syncbridge.colombodeals.models.Deal;
import com.syncbridge.colombodeals.models.DealDetail;
import com.syncbridge.colombodeals.models.PaymentCard;
import com.syncbridge.colombodeals.models.Review;
import com.syncbridge.colombodeals.models.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import io.techery.properratingbar.ProperRatingBar;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DealDetailActivity extends AppCompatActivity implements OnItemListener,
        MaterialDialog.SingleButtonCallback, OnMapReadyCallback {

    protected static final String TAG = DealDetailActivity.class.getSimpleName();
    private static final int MY_PERMISSIONS_REQUEST_MAPS_RECEIVE = 533;
    /**
     * The {@code FirebaseAnalytics} used to record screen views.
     */
    // [START declare_analytics]
    private FirebaseAnalytics mFirebaseAnalytics;
    // [END declare_analytics]
    private Context mContext;
    private ProgressBar mProgressBar;
    private ResponseErrorMessageHandler mREM;
    private String mDealId;
    private List<PaymentCard> mCards = new ArrayList<>();
    private List<Review> mComments = new ArrayList<>();
    private CardDetailAdapter mCardDetailAdapter;
    private CommentAdapter mCommentAdapter;
    private boolean mMapsReadyToGo = false;
    private GoogleMap mMap;
    private Deal mDeal;

    private MapFragment mMapFragment;
    private View mBaseView;
    private View mSliderBase;
    private AppCompatTextView mTitle;
    private AppCompatTextView mCategory;
    private AppCompatTextView mHighlightSection;
    private AppCompatTextView mHighlight;
    private ProperRatingBar mRatingBar;
    private AppCompatImageView mCategoryImage;
    private View mAddressBase;
    private AppCompatTextView mAddress;
    private View mPhoneBase;
    private AppCompatTextView mPhone;
    private View mWebBase;
    private AppCompatTextView mWeb;
    private AppCompatTextView mLocationSection;
    private AppCompatTextView mCommentSection;

    //region functions

    private void getDeal(String dealId) {

        if (!Functions.IsInternetOn(mContext)) {
            mREM.showRetry(Functions.GetStringResource(mContext, R.string.error_internet_not_available), -1, null, null);
            return;
        }

        if (dealId == null || dealId.isEmpty()) {
            new MaterialDialog.Builder(mContext)
                    .title(Functions.GetStringResource(mContext, R.string.dialog_title_error_response))
                    .content("Sorry unable to gather data.\nPlease restart the application.")
                    .positiveText(R.string.dialog_button_ok)
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            finish();
                            dialog.dismiss();
                        }
                    })
                    .show();
            return;
        }

        mProgressBar.start();
        ApiUsage.GetDeal(dealId, new Callback<DealDetail>() {
            @Override
            public void onResponse(Call<DealDetail> call, Response<DealDetail> response) {
                if (response.isSuccessful()) {
                    switch (response.body().getStatus()) {
                        case 1:
                            fetchData(response.body().getDeal());
                            break;
                        default:
                            Log.e(TAG, response.body().getMsg());
                            break;
                    }
                    mProgressBar.stop();
                } else {
                    mProgressBar.stop();
                    mREM.showRetry(response.body().getMsg(), response.code(), null, null);
                }
            }

            @Override
            public void onFailure(Call<DealDetail> call, Throwable t) {
                Log.e(TAG, t.toString());
                mProgressBar.stop();
                mREM.showRetry(null, -1, t, null);
            }
        });
    }

    private void fetchData(Deal deal) {
        mDeal = deal;
        fetchMap();
        fetchBanners(mDeal.getImage());
        mTitle.setText(mDeal.getTitle());
        mCategory.setText(mDeal.getCategory());
        mHighlightSection.setText(R.string.text_highlights_section);
        mHighlight.setText(mDeal.getHighlights());
        mRatingBar.setRating(mDeal.getRating().intValue());
        mLocationSection.setText(R.string.text_location_section);
        mCommentSection.setText(R.string.text_comments_section);

        switch (mDeal.getCategory().toLowerCase()) {
            case "restaurant":
                mCategoryImage.setImageDrawable(Functions.GetDrawable(mContext, R.drawable.restaurant_blue));
                break;
            case "leisure":
                mCategoryImage.setImageDrawable(Functions.GetDrawable(mContext, R.drawable.leisure_blue));
                break;
            case "travel":
                mCategoryImage.setImageDrawable(Functions.GetDrawable(mContext, R.drawable.travel_blue));
                break;
            case "fashion":
                mCategoryImage.setImageDrawable(Functions.GetDrawable(mContext, R.drawable.fashion_blue));
                break;
            default:
                mCategoryImage.setVisibility(View.GONE);
                break;
        }

        if (deal.getLocation() != null && !deal.getLocation().isEmpty()) {
            mAddress.setText(deal.getLocation());
        } else {
            mAddressBase.setVisibility(View.GONE);
        }

        if (deal.getPhone() != null && !deal.getPhone().isEmpty()) {
            mPhone.setText(deal.getPhone());
        } else {
            mPhoneBase.setVisibility(View.GONE);
        }

        if (deal.getWeb() != null && !deal.getWeb().isEmpty()) {
            mWeb.setText(deal.getWeb());
        } else {
            mWebBase.setVisibility(View.GONE);
        }

        mCards.clear();
        mCards.addAll(mDeal.getPaymentCards());
        mCardDetailAdapter.notifyDataSetChanged();

        mComments.clear();
        mComments.addAll(mDeal.getReviews());
        mCommentAdapter.notifyDataSetChanged();

        if (mComments.size() == 0)
            mCommentSection.setVisibility(View.GONE);

        mBaseView.setVisibility(View.VISIBLE);
    }

    private void fetchBanners(String url) {
        SliderLayout mSlider = (SliderLayout) findViewById(R.id.slider);

        if (mSlider == null)
            return;

        mSlider.setPresetTransformer(SliderLayout.Transformer.Fade);
        mSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Top);
        mSlider.setCustomAnimation(new DescriptionAnimation());
        mSlider.setCustomIndicator((PagerIndicator) findViewById(R.id.custom_indicator));
        mSlider.setDuration(4000);

        HashMap<String, String> url_maps = new HashMap<>();

        if (url != null && !url.isEmpty()) {
            int index = 0;
            url_maps.put("ban" + String.valueOf(index), url);
//            for (String url : mDealObject.images) {
//                if (url != null) {
//                    url_maps.put("ban" + String.valueOf(index), url);
//                    if (BuildConfig.DEBUG) {
//                        Log.d(TAG, url);
//                    }
//                    index++;
//                }
//            }
        }

        if (!url_maps.isEmpty()) {
            for (String name : url_maps.keySet()) {
                DefaultSliderView defaultSliderView = new DefaultSliderView(mContext);
                defaultSliderView
                        .image(url_maps.get(name))
                        .setScaleType(BaseSliderView.ScaleType.CenterInside);
//                defaultSliderView.empty(R.drawable.image_placeholder);
//                defaultSliderView.error(R.drawable.image_placeholder);

                defaultSliderView.bundle(new Bundle());
                defaultSliderView.getBundle()
                        .putString("extra", name);

                mSlider.addSlider(defaultSliderView);
            }

            mSlider.startAutoCycle();
            mSliderBase.setVisibility(View.VISIBLE);
        } else {
            mSliderBase.setVisibility(View.GONE);
        }
    }

    private void fetchMap() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(DealDetailActivity.this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_MAPS_RECEIVE);
            return;
        }

        if (!mMapsReadyToGo || mDeal == null || mDeal.getLat() == null || mDeal.getLon() == null) {
            mLocationSection.setVisibility(View.GONE);
            if (mMapFragment != null && mMapFragment.getView() != null)
                mMapFragment.getView().setVisibility(View.GONE);
            return;
        } else {
            mLocationSection.setVisibility(View.VISIBLE);
            if (mMapFragment != null && mMapFragment.getView() != null)
                mMapFragment.getView().setVisibility(View.VISIBLE);
        }

        LatLng mLatLng = new LatLng(mDeal.getLat(), mDeal.getLon());

        mMap.setMyLocationEnabled(true);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mLatLng, 13));

        mMap.addMarker(new MarkerOptions()
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.location_blue))
                .title(mDeal.getMerchant())
                .snippet(mDeal.getLocation())
                .position(mLatLng));
    }

    private void addReview(int rating, String comment) {
//        if (!Functions.IsInternetOn(mContext)) {
//            mREM.showRetry(Functions.GetStringResource(mContext, R.string.error_internet_not_available), -1, null, null);
//            return;
//        }

        mProgressBar.start();

        User mUser = Preference.getInstance(mContext).getCurrentUser();

        ApiUsage.AddReview(mDealId, comment, rating, Integer.parseInt(mUser.customer_id), mUser.name, mUser.token,
                new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful() && response.code() == 200) {
                            try {
                                JSONObject object = new JSONObject(response.body().string());
                                int status = object.getInt("status");
                                String msg = object.getString("msg");

                                Toast.makeText(mContext, msg, Toast.LENGTH_LONG).show();
                            } catch (IOException | JSONException e) {
                                Log.e(TAG, e.toString());
                            }
                        }
                        mProgressBar.stop();
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.e(TAG, t.toString());
                        mProgressBar.stop();
//                mREM.showRetry(null, -1, t, null);
                    }
                });
    }

    private void showAddComment() {
        View mView = LayoutInflater.from(mContext).inflate(R.layout.layout_add_comment, null, false);
        final ProperRatingBar mRating = (ProperRatingBar) mView.findViewById(R.id.rating);
        final MaterialEditText mComment = (MaterialEditText) mView.findViewById(R.id.et_comment);
        AppCompatButton mSubmit = (AppCompatButton) mView.findViewById(R.id.btn_submit);

        final MaterialDialog dialog = new MaterialDialog.Builder(mContext)
                .title("Add Comment")
                .customView(mView, false)
                .build();

        mComment.addValidator(Functions.ValidationRequired(mContext));
        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mComment.validate()) {
                    dialog.dismiss();
                    addReview(mRating.getRating(), mComment.getText().toString());
                }
            }
        });

        dialog.show();
    }

    private void share() {
        // Create a deep link and display it in the UI
        final Uri deepLink = Functions.BuildDeepLink(mContext,
                Uri.parse(String.format(Locale.getDefault(),
                        Functions.GetStringResource(mContext, R.string.deep_link_url), mDeal.getId())),
                0, false);
        Log.d(TAG, "Deep Link", deepLink.toString());

        Functions.ShareDeepLink(mContext, mDeal.getTitle(), deepLink.toString());

        // [START custom_event]
        Bundle params = new Bundle();
        params.putString("deal_id", mDeal.getId());
        params.putString("deal_title", mDeal.getTitle());
        mFirebaseAnalytics.logEvent("share_deal", params);
        // [END custom_event]
    }
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deal_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("");
        }

        mContext = DealDetailActivity.this;

        // [START shared_app_measurement]
        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        // [END shared_app_measurement]

        mProgressBar = ProgressBar.getInstance(findViewById(R.id.progress_container));
        mREM = new ResponseErrorMessageHandler(mContext, this);
        mREM.cancelable(false);
        mREM.showCancelButton(true);

        if (getIntent() != null && getIntent().hasExtra(Preference.Keys.DEAL_ID.name())) {
            mDealId = getIntent().getExtras().getString(Preference.Keys.DEAL_ID.name());
        }

        mBaseView = findViewById(R.id.base_view);
        if (mBaseView != null)
            mBaseView.setVisibility(View.INVISIBLE);

        mSliderBase = findViewById(R.id.slider_base);
        if (mSliderBase != null)
            mSliderBase.setVisibility(View.GONE);

        mTitle = (AppCompatTextView) findViewById(R.id.tv_title);
        mCategory = (AppCompatTextView) findViewById(R.id.tv_category);
        mHighlightSection = (AppCompatTextView) findViewById(R.id.tv_highlight_section);
        mHighlight = (AppCompatTextView) findViewById(R.id.tv_highlight);
        mRatingBar = (ProperRatingBar) findViewById(R.id.rating);
        mAddress = (AppCompatTextView) findViewById(R.id.address);
        mAddressBase = findViewById(R.id.address_base);
        mPhone = (AppCompatTextView) findViewById(R.id.phone);
        mPhoneBase = findViewById(R.id.phone_base);
        mWeb = (AppCompatTextView) findViewById(R.id.web);
        mWebBase = findViewById(R.id.web_base);
        mCategoryImage = (AppCompatImageView) findViewById(R.id.image_category);
        mLocationSection = (AppCompatTextView) findViewById(R.id.tv_location_section);
        mCommentSection = (AppCompatTextView) findViewById(R.id.tv_comments_section);
        AppCompatButton mAddComment = (AppCompatButton) findViewById(R.id.btn_add_comment);
        if (mAddComment != null) {
            mAddComment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showAddComment();
                }
            });
        }

        mCardDetailAdapter = new CardDetailAdapter(mContext, mCards);

        RecyclerView mCardsRecyclerView = (RecyclerView) findViewById(R.id.rv_cards);
        if (mCardsRecyclerView != null) {
            mCardsRecyclerView.setHasFixedSize(true);
            mCardsRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
            mCardsRecyclerView.setAdapter(mCardDetailAdapter);
        }

        mCommentAdapter = new CommentAdapter(mContext, mComments);

        RecyclerView mCommentsRecyclerView = (RecyclerView) findViewById(R.id.rv_comments);
        if (mCommentsRecyclerView != null) {
            mCommentsRecyclerView.setHasFixedSize(true);
            mCommentsRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
            mCommentsRecyclerView.setAdapter(mCommentAdapter);
        }

        getDeal(mDealId);

        mMapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mMapFragment.getMapAsync(this);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putString(SaveInstanceStates.DEAL_ID.name(), mDealId);

        super.onSaveInstanceState(savedInstanceState);
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        mDealId = savedInstanceState.getString(SaveInstanceStates.DEAL_ID.name());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_deal, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_share) {
            share();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemListener(View v) {
    }

    @Override
    public void onItemListener(View v, int region) {

    }

    @Override
    public void onItemListener(int position, Identity identity, Object object) {

    }

    @Override
    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
        switch (which) {
            case NEGATIVE:
                finish();
                dialog.dismiss();
                break;
            case POSITIVE:
                dialog.dismiss();
                getDeal(mDealId);
                break;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d(TAG, "Map Ready");
        mMap = googleMap;

        if (ContextCompat.checkSelfPermission(
                DealDetailActivity.this,
                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(DealDetailActivity.this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_MAPS_RECEIVE);
        } else {
            mMapsReadyToGo = true;
            fetchMap();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_MAPS_RECEIVE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mMapsReadyToGo = true;
                    fetchMap();
                } else {
                    mLocationSection.setVisibility(View.GONE);

                    if (mMapFragment != null && mMapFragment.getView() != null)
                        mMapFragment.getView().setVisibility(View.GONE);
                }
            }
        }
    }

//    @Override
//    public void onResponseCallBack(int id, @NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//        switch (id) {
//            case 0:
//                switch (which) {
//                    case NEGATIVE:
//                        finish();
//                        dialog.dismiss();
//                        break;
//                    case POSITIVE:
//                        dialog.dismiss();
//                        getDeal(mDealId);
//                        break;
//                }
//                break;
//        }
//    }
}
