package com.syncbridge.colombodeals.views.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rengwuxian.materialedittext.validation.METValidator;
import com.syncbridge.colombodeals.R;
import com.syncbridge.colombodeals.api.ApiUsage;
import com.syncbridge.colombodeals.common.Functions;
import com.syncbridge.colombodeals.common.Identity;
import com.syncbridge.colombodeals.common.Log;
import com.syncbridge.colombodeals.common.ProgressBar;
import com.syncbridge.colombodeals.common.ResponseErrorMessageHandler;
import com.syncbridge.colombodeals.interfaces.OnItemListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignupActivity extends AppCompatActivity implements OnItemListener, MaterialDialog.SingleButtonCallback {

    private static final String TAG = SignupActivity.class.getSimpleName();
    private Context mContext;
    private ProgressBar mProgressBar;
    private ResponseErrorMessageHandler mREM;

    private MaterialEditText mFullName, mEmail, mPassword, mCPassword;

    //region functions

    private void createUser() {

        if (!Functions.IsInternetOn(mContext)) {
            mREM.showRetry(Functions.GetStringResource(mContext, R.string.error_internet_not_available), -1, null, null);
            return;
        }

        mProgressBar.start();

        ApiUsage.UserCreate(mEmail.getText().toString(), mPassword.getText().toString(),
                mFullName.getText().toString(), new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        try {
                            String jsonString = response.body().string();
                            Log.d(TAG, jsonString);

                            JSONObject mObject = new JSONObject(jsonString);

                            if (mObject != null) {
                                switch (mObject.getInt("status")) {
                                    case 0:
                                        Toast.makeText(mContext, mObject.getString("msg"), Toast.LENGTH_LONG).show();
                                        break;
                                    case 1:
                                        Toast.makeText(mContext, mObject.getString("msg"), Toast.LENGTH_LONG).show();
                                        Intent returnIntent = new Intent();
                                        returnIntent.putExtra("email", mEmail.getText().toString());
                                        returnIntent.putExtra("password", mPassword.getText().toString());
                                        returnIntent.putExtra("name", mFullName.getText().toString());
                                        setResult(Activity.RESULT_OK, returnIntent);
                                        finish();
                                        break;
                                }
                            }

                        } catch (IOException e) {
                            Log.e(TAG, e.toString());
                        } catch (JSONException e) {
                            Log.e(TAG, e.toString());
                        }

                        mProgressBar.stop();
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        mProgressBar.stop();
                        Log.d(TAG, t.toString());
                    }
                });
    }

    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        mContext = SignupActivity.this;

        mProgressBar = ProgressBar.getInstance(findViewById(R.id.progress_container));
        mREM = new ResponseErrorMessageHandler(mContext, this);
        mREM.cancelable(false);
        mREM.showCancelButton(true);

        mFullName = (MaterialEditText) findViewById(R.id.name);
        mEmail = (MaterialEditText) findViewById(R.id.email);
        mPassword = (MaterialEditText) findViewById(R.id.password);
        mCPassword = (MaterialEditText) findViewById(R.id.c_password);

        mFullName.addValidator(Functions.ValidationRequired(mContext));

        mEmail.addValidator(Functions.ValidationRequired(mContext));
        mEmail.addValidator(Functions.ValidationEmail(mContext));

        mPassword.addValidator(Functions.ValidationRequired(mContext));
        mCPassword.addValidator(Functions.ValidationRequired(mContext));
        mCPassword.addValidator(new METValidator(Functions.GetStringResource(mContext, R.string.error_password_not_match)) {
            @Override
            public boolean isValid(@NonNull CharSequence text, boolean isEmpty) {
                return !isEmpty && Functions.IsValidPassword(mPassword.getText().toString(), text);
            }
        });
    }

    @Override
    public void onItemListener(View v) {
        switch (v.getId()) {
            case R.id.sign_up:
                if (mFullName.validate()
                        && mEmail.validate()
                        && mPassword.validate()
                        && mCPassword.validate()) {
                    if (((AppCompatCheckBox) findViewById(R.id.checkbox)).isChecked()) {
                        createUser();
                    } else {
                        Toast.makeText(mContext, "Please accept terms and conditions", Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }

    @Override
    public void onItemListener(View v, int region) {

    }

    @Override
    public void onItemListener(int position, Identity identity, Object object) {

    }

    @Override
    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
        switch (which) {
            case NEGATIVE:
                dialog.dismiss();
                break;
            case POSITIVE:
                dialog.dismiss();
                createUser();
                break;
        }
    }
}
