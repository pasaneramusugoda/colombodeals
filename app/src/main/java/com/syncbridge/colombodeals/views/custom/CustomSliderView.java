package com.syncbridge.colombodeals.views.custom;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.syncbridge.colombodeals.R;
import com.syncbridge.colombodeals.common.Identity;
import com.syncbridge.colombodeals.interfaces.OnItemListener;
import com.syncbridge.colombodeals.models.Banner;

/**
 * Created by Pasan Eramusugoda on 4/23/2016.
 */
public class CustomSliderView extends BaseSliderView {

    private Banner mBanner;
    private int mPosition;
    private OnItemListener mListener;

    public CustomSliderView(Context context, int position, Banner banner, OnItemListener listener) {
        super(context);
        mBanner = banner;
        mPosition = position;
        mListener = listener;
    }

    @Override
    public View getView() {
        View v = LayoutInflater.from(getContext()).inflate(R.layout.layout_slider_item, null);
        ImageView target = (ImageView) v.findViewById(R.id.daimajia_slider_image);

        if (mBanner.getDealId() == null || mBanner.getDealId() == null || mBanner.getDealId().isEmpty()) {
            v.findViewById(R.id.view_deal).setVisibility(View.GONE);
        } else {
            v.findViewById(R.id.view_deal).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null)
                        mListener.onItemListener(mPosition, Identity.SLIDER, mBanner);
                }
            });
        }
        bindEventAndShow(v, target);
        return v;
    }
}
