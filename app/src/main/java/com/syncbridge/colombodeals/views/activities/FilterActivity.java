package com.syncbridge.colombodeals.views.activities;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.syncbridge.colombodeals.R;
import com.syncbridge.colombodeals.adapters.MerchantAdapter;
import com.syncbridge.colombodeals.api.ApiUsage;
import com.syncbridge.colombodeals.common.Functions;
import com.syncbridge.colombodeals.common.Identity;
import com.syncbridge.colombodeals.common.Log;
import com.syncbridge.colombodeals.common.ProgressBar;
import com.syncbridge.colombodeals.common.ResponseErrorMessageHandler;
import com.syncbridge.colombodeals.interfaces.OnItemListener;
import com.syncbridge.colombodeals.models.Merchant;
import com.syncbridge.colombodeals.models.Merchants;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FilterActivity extends AppCompatActivity implements MaterialDialog.SingleButtonCallback, OnItemListener, View.OnClickListener, DatePickerDialog.OnDateSetListener {

    private static final String TAG = FilterActivity.class.getSimpleName();
    private Context mContext;
    private ProgressBar mProgressBar;
    private ResponseErrorMessageHandler mREM;
    private List<Merchant> mMerchants = new ArrayList<>();
    private MerchantAdapter mMerchantAdapter;
    private List<String> mSelectedMerchants = new ArrayList<>();
    private String mStartDate;
    private String mEndDate;

    private AppCompatButton mSDate;
    private AppCompatButton mEDate;

    //region imp

    private void clear() {
        mSelectedMerchants.clear();
        mMerchantAdapter.unselectAll();
        mStartDate =null;
        mEndDate = null;
        mSDate.setText(Functions.GetStringResource(mContext, R.string.button_start_date));
        mEDate.setText(Functions.GetStringResource(mContext, R.string.button_end_date));
    }

    private void filter() {
        mSelectedMerchants.clear();
        mSelectedMerchants.addAll(mMerchantAdapter.getSelectedMerchantNames());

        StringBuilder merchants = new StringBuilder();
        for (int i = 0; i < mSelectedMerchants.size(); i++) {
            if(i + 1 == mSelectedMerchants.size()) {
                merchants.append(String.format("merchant[%s]=%s", i, mSelectedMerchants.get(i)));
            } else {
                merchants.append(String.format("merchant[%s]=%s&", i, mSelectedMerchants.get(i)));
            }
        }

        Intent intent = new Intent(FilterActivity.this, FilterResultActivity.class);
        intent.putExtra("merchants", mSelectedMerchants.size() > 0 ? merchants.toString() : null);
        intent.putExtra("startDate", mStartDate);
        intent.putExtra("endDate", mEndDate);
        startActivity(intent);
    }

    private void showDatePicker(String tag) {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.show(getFragmentManager(), tag);
    }

    private void getMerchants() {

        if (!Functions.IsInternetOn(mContext)) {
            mREM.showRetry(Functions.GetStringResource(mContext, R.string.error_internet_not_available), -1, null, null);
            return;
        }

            mProgressBar.start();

        ApiUsage.GetMerchants(new Callback<Merchants>() {
            @Override
            public void onResponse(Call<Merchants> call, Response<Merchants> response) {
                if (response.isSuccessful()) {
                    switch (response.body().getStatus()) {
                        case 1:
                            mMerchants.clear();
                            mMerchants.addAll(response.body().getMerchants());
                            mMerchantAdapter.notifyDataSetChanged();
                            break;
                        default:
                            Log.e(TAG, response.body().getMsg());
                            break;
                    }
                    mProgressBar.stop();
                } else {
                    mProgressBar.stop();
                    mREM.showRetry(response.body().getMsg(), response.code(), null, null);
                }
            }

            @Override
            public void onFailure(Call<Merchants> call, Throwable t) {
                Log.e(TAG, t.toString());
                mProgressBar.stop();
                mREM.showRetry(null, -1, t, null);
            }
        });
    }

    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        mContext = FilterActivity.this;
        mProgressBar = ProgressBar.getInstance(findViewById(R.id.progress_container));
        mREM = new ResponseErrorMessageHandler(mContext, this);
        mREM.cancelable(false);
        mREM.showCancelButton(true);

        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.rv_banks);
        if (mRecyclerView == null) {
            return;
        }

        mRecyclerView.setHasFixedSize(true);

        mMerchantAdapter = new MerchantAdapter(mContext, mMerchants, this);
        mRecyclerView.setAdapter(mMerchantAdapter);

        StaggeredGridLayoutManager mGridLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mGridLayoutManager);

        getMerchants();

        mSDate = (AppCompatButton) findViewById(R.id.button_start_date);
        mSDate.setOnClickListener(this);
        mEDate = (AppCompatButton) findViewById(R.id.button_end_date);
        mEDate.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_filter, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_done) {
            filter();
            return true;
        }

        if(id == R.id.action_clear) {
            clear();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
        switch (which) {
            case NEGATIVE:
                dialog.dismiss();
                break;
            case POSITIVE:
                dialog.dismiss();
                getMerchants();
                break;
        }
    }

    @Override
    public void onItemListener(View v) {

    }

    @Override
    public void onItemListener(View v, int region) {

    }

    @Override
    public void onItemListener(int position, Identity identity, Object object) {
        if(identity == Identity.MERCHANT) {
            mMerchantAdapter.selectPosition(position);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_start_date:
                showDatePicker("start");
                break;
            case R.id.button_end_date:
                showDatePicker("end");
                break;
        }
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        switch (view.getTag()) {
            case "start":
                mStartDate = String.valueOf(year + "-" + String.format(Locale.getDefault(), "%02d", monthOfYear + 1) + "-" + String.format(Locale.getDefault(), "%02d", dayOfMonth));
                mSDate.setText(Functions.GetStringResource(mContext, R.string.button_start_date) + " : " + mStartDate);
                break;
            case "end":
                mEndDate = String.valueOf(year + "-" + String.format(Locale.getDefault(), "%02d", monthOfYear + 1) + "-" + String.format(Locale.getDefault(), "%02d", dayOfMonth));
                mEDate.setText(Functions.GetStringResource(mContext, R.string.button_end_date) + " : " + mEndDate);
                break;
        }
    }
}
