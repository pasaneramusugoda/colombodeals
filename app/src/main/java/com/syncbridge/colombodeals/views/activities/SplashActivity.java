package com.syncbridge.colombodeals.views.activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.google.android.gms.appinvite.AppInvite;
import com.google.android.gms.appinvite.AppInviteInvitationResult;
import com.google.android.gms.appinvite.AppInviteReferral;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.syncbridge.colombodeals.R;
import com.syncbridge.colombodeals.common.Functions;
import com.syncbridge.colombodeals.common.Log;
import com.syncbridge.colombodeals.common.Preference;
import com.syncbridge.colombodeals.models.User;

import java.util.Date;

public class SplashActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, ResultCallback<AppInviteInvitationResult> {

    private static final String TAG = SplashActivity.class.getSimpleName();
    private GoogleApiClient mGoogleApiClient;
    private Context mContext;
    private User user;

    //region functions

    private boolean isLoggedIn() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken != null;
    }

    private boolean checkUser(Context context) {
        User user = Preference.getInstance(context).getCurrentUser();

        if (user == null) {
            return false;
        } else {
            switch (user.login_type) {
                case NORMAL:
                    if (user.token_expire_at == null || user.token_expire_at.isEmpty()
                            || user.customer_id == null || user.customer_id.isEmpty()) {
                        return false;
                    } else {
                        Date dateExpire = Functions.GetDateTime(user.token_expire_at, "yyyy-MM-dd HH:mm:ss");
                        Date dateNow = new Date();
                        return dateExpire != null && Functions.GetDateTimeDifference(dateNow, dateExpire) > 0;
                    }
                case FB:
                    return isLoggedIn();
                case GOOGLE:
                    //TODO: implement
                    return true;
                default:
                    return false;
            }
        }
    }

    private void continue_process() {
        if (user == null || checkUser(mContext)) {
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            }, 3000);
        } else {
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
            }, 3000);
        }
    }

    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        mContext = SplashActivity.this;
        user = Preference.getInstance(mContext).getCurrentUser();

        //region dynamic links

        // [START build_api_client]
        // Build GoogleApiClient with AppInvite API for receiving deep links
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .enableAutoManage(this, this)
                    .addApi(AppInvite.API)
                    .build();
        }
        // [END build_api_client]

        // [START get_deep_link]
        // Check if this app was launched from a deep link. Setting autoLaunchDeepLink to true
        // would automatically launch the deep link if one is found.
        boolean autoLaunchDeepLink = false;
        AppInvite.AppInviteApi.getInvitation(mGoogleApiClient, this, autoLaunchDeepLink).setResultCallback(this);
        // [END get_deep_link]

        //endregion
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.w(TAG, "onConnectionFailed:" + connectionResult);
        Toast.makeText(this, "Google Play Services Error: " + connectionResult.getErrorCode(),
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResult(@NonNull AppInviteInvitationResult appInviteInvitationResult) {
        if (appInviteInvitationResult.getStatus().isSuccess()) {
            // Extract deep link from Intent
            Intent intent = appInviteInvitationResult.getInvitationIntent();
            String deepLink = AppInviteReferral.getDeepLink(intent);
            Uri deepLinkUri = Uri.parse(deepLink);
            String lastPath = deepLinkUri.getLastPathSegment();

            switch (lastPath) {
                case "deal":
                    String dealId = deepLinkUri.getQueryParameter("id");
                    if (dealId != null && !dealId.isEmpty()) {
                        Intent _intent = new Intent(this, DealDetailActivity.class);
                        _intent.putExtra(Preference.Keys.DEAL_ID.name(), dealId);
                        startActivity(_intent);
                        finish();
                    }
                    break;
                default:
                    continue_process();
                    break;
            }
            Log.d(TAG, "deep link", deepLink);
        } else {
            Log.d(TAG, "getInvitation: no deep link found.");
            continue_process();
        }
    }
}
