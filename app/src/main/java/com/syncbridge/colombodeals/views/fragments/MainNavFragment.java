package com.syncbridge.colombodeals.views.fragments;


import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.syncbridge.colombodeals.R;
import com.syncbridge.colombodeals.adapters.MainNavAdapter;
import com.syncbridge.colombodeals.common.DividerItemDecoration;
import com.syncbridge.colombodeals.common.Functions;
import com.syncbridge.colombodeals.common.Log;
import com.syncbridge.colombodeals.common.Preference;
import com.syncbridge.colombodeals.interfaces.NavigationDrawerCallbacks;
import com.syncbridge.colombodeals.interfaces.OnItemListener;
import com.syncbridge.colombodeals.models.MainNavItem;
import com.syncbridge.colombodeals.models.MainNavItems;
import com.syncbridge.colombodeals.models.User;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainNavFragment extends Fragment implements NavigationDrawerCallbacks {
    private static final String TAG = MainNavFragment.class.getSimpleName();

    /**
     * Remember the position of the selected item.
     */
    private static final String STATE_SELECTED_POSITION = "selected_navigation_drawer_position";
    /**
     * Per the design guidelines, you should show the drawer on launch until the user manually
     * expands it. This shared preference tracks this.
     */
    private static final String PREF_USER_LEARNED_DRAWER = "navigation_drawer_learned";
    private static int mCurrentSelectedPosition = 0;
    private static ActionBarDrawerToggle mActionBarDrawerToggle;
    private static NavigationDrawerCallbacks mCallbacks;
    private static RecyclerView mDrawerList;
    private static View mView;
    private static View mFragmentContainerView;
    private static DrawerLayout mDrawerLayout;

    private static Context mContext;
    private static User mUser;
    private static List<MainNavItem> navigationItems;
    private static MainNavAdapter adapter;

    private boolean mFromSavedInstanceState;
    private boolean mUserLearnedDrawer;

    private OnItemListener mListener;

    public MainNavFragment() {
        // Required empty public constructor
    }

    public static boolean isDrawerOpen() {
        return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(mFragmentContainerView);
    }

    public static void closeDrawer() {
        mDrawerLayout.closeDrawer(mFragmentContainerView);
    }

    private static void userLogged() {
        mUser = Preference.getInstance(mContext).getCurrentUser();
//
        if (mUser != null) {
            Log.d(TAG, "****IN****");
//            TextView email = (TextView) mView.findViewById(R.id.profile_email_text);
            TextView name = (TextView) mView.findViewById(R.id.profile_name_text);

//            email.setTypeface(Functions.GetTypeFace(mContext, FontType.MontserratLight));
//            name.setTypeface(Functions.GetTypeFace(mContext, FontType.MontserratLight));

//            email.setText(mUser.email);
            name.setText(mUser.name);

//            ((CircleImageView)mView.findViewById(R.id.icon)).setImageDrawable();
            if (mUser.profile_pic_url == null || mUser.profile_pic_url.isEmpty()) {
                ((CircleImageView) mView.findViewById(R.id.icon)).setImageDrawable(
                        Functions.GetDrawable(mContext, R.mipmap.ic_launcher)
                );
            } else {
                Picasso.with(mContext)
                        .load(mUser.profile_pic_url)
                        .placeholder(Functions.GetDrawable(mContext, R.drawable.profile_picture))
                        .error(Functions.GetDrawable(mContext, R.mipmap.ic_launcher))
                        .into((CircleImageView) mView.findViewById(R.id.icon));
            }

            mView.findViewById(R.id.user_login).setVisibility(View.GONE);
            mView.findViewById(R.id.user_info).setVisibility(View.VISIBLE);
        } else {
            Log.d(TAG, "****OUT****");
            ((CircleImageView) mView.findViewById(R.id.icon)).setImageDrawable(
                    Functions.GetDrawable(mContext, R.mipmap.ic_launcher)
            );
            mView.findViewById(R.id.user_login).setVisibility(View.VISIBLE);
            mView.findViewById(R.id.user_info).setVisibility(View.GONE);
        }
    }

    public static void selectItem(int position) {
        mCurrentSelectedPosition = position;
        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawer(mFragmentContainerView);
        }
        if (mCallbacks != null) {
            mCallbacks.onNavigationDrawerItemSelected(position);
        }
        ((MainNavAdapter) mDrawerList.getAdapter()).selectPosition(position);
    }

    public static void updateAdapter(boolean isUserLogged) {
        userLogged();

        for (MainNavItem item : navigationItems) {
            if (!item.getText().equals(MainNavItems.MY_ACCOUNT.name))
                item.setEnable(isUserLogged);
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Read in the flag indicating whether or not the user has demonstrated awareness of the
        // drawer. See PREF_USER_LEARNED_DRAWER for details.
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mUserLearnedDrawer = sp.getBoolean(PREF_USER_LEARNED_DRAWER, false);

        if (savedInstanceState != null) {
            mCurrentSelectedPosition = savedInstanceState.getInt(STATE_SELECTED_POSITION);
            mFromSavedInstanceState = true;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.fragment_main_nav, container, false);

        TextView mCompany = (TextView) mView.findViewById(R.id.company);
        TextView mVersion = (TextView) mView.findViewById(R.id.version);

//        mCompany.setTypeface(Functions.GetTypeFace(mContext, FontType.MontserratSemiBold));
//        mVersion.setTypeface(Functions.GetTypeFace(mContext, FontType.MontserratSemiBold));
        if (mContext == null) {
            mContext = mView.getContext();
        }

        PackageInfo pInfo = null;
        try {
            pInfo = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        if (pInfo != null) {
            mVersion.setText("v" + pInfo.versionName);
        } else {
            mCompany.setPadding(0, 5, 0, 5);
            mVersion.setVisibility(View.GONE);
        }


        mDrawerList = (RecyclerView) mView.findViewById(R.id.navdrawer_items_list);

        userLogged();

        mView.findViewById(R.id.sign_in).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isDrawerOpen())
                    closeDrawer();

                onItemListener(v);
            }
        });

        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

        mDrawerList.setLayoutManager(layoutManager);
        mDrawerList.setHasFixedSize(true);

        mDrawerList.addItemDecoration(new DividerItemDecoration(getResources().getDrawable(R.drawable.main_nav_divider)));

        navigationItems = getMenu(mUser != null);
        adapter = new MainNavAdapter(navigationItems);
        adapter.setNavigationDrawerCallbacks(this);
        mDrawerList.setAdapter(adapter);

        return mView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mContext = context;

        try {
            mCallbacks = (NavigationDrawerCallbacks) context;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement NavigationDrawerCallbacks.");
        }

        try {
            mListener = (OnItemListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement OnItemListener.");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        selectItem(position);
    }

    public DrawerLayout getDrawerLayout() {
        return mDrawerLayout;
    }

    public void setDrawerLayout(DrawerLayout drawerLayout) {
        mDrawerLayout = drawerLayout;
    }

    private List<MainNavItem> getMenu(boolean isUserLogged) {
        List<MainNavItem> items = new ArrayList<>();

        items.add(new MainNavItem(MainNavItems.MY_ACCOUNT.title, R.drawable.my_account, isUserLogged));
        items.add(new MainNavItem(MainNavItems.SUBSCRIBE.title, R.drawable.subscribe, true));
        items.add(new MainNavItem(MainNavItems.NOTIFICATION.title, R.drawable.notification, isUserLogged));
        items.add(new MainNavItem(MainNavItems.CONTACT_US.title, R.drawable.contact_us, true));
        items.add(new MainNavItem(MainNavItems.ABOUT_US.title, R.drawable.about_us, true));
        items.add(new MainNavItem(MainNavItems.LOG_OUT.title, R.drawable.logout, isUserLogged));

        return items;
    }

    public void setup(NavigationDrawerCallbacks navigationDrawerCallbacks, OnItemListener onItemListener, DrawerLayout drawerLayout, Toolbar toolbar) {
        mCallbacks = navigationDrawerCallbacks;
        mListener = onItemListener;

        mFragmentContainerView = getActivity().findViewById(R.id.fragment_drawer);
        mDrawerLayout = drawerLayout;
        mActionBarDrawerToggle = new ActionBarDrawerToggle(getActivity(), mDrawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                if (!isAdded()) return;
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                if (!isAdded()) return;

                if (!mUserLearnedDrawer) {
                    // The user manually opened the drawer; store this flag to prevent auto-showing
                    // the navigation drawer automatically in the future.
                    mUserLearnedDrawer = true;
                    SharedPreferences sp = PreferenceManager
                            .getDefaultSharedPreferences(getActivity());
                    sp.edit().putBoolean(PREF_USER_LEARNED_DRAWER, true).apply();
                }

                getActivity().invalidateOptionsMenu();
            }
        };

        // If the user hasn't 'learned' about the drawer, open it to introduce them to the drawer,
        // per the navigation drawer design guidelines.
        if (!mUserLearnedDrawer && !mFromSavedInstanceState) {
            mDrawerLayout.openDrawer(mFragmentContainerView);
        }

        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mActionBarDrawerToggle.syncState();
            }
        });

        mDrawerLayout.setDrawerListener(mActionBarDrawerToggle);

    }

    private void onItemListener(View v) {
        if (mListener != null) {
            mListener.onItemListener(v);
        }
    }

}
