package com.syncbridge.colombodeals.views.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.syncbridge.colombodeals.R;
import com.syncbridge.colombodeals.api.ApiUsage;
import com.syncbridge.colombodeals.common.Functions;
import com.syncbridge.colombodeals.common.Identity;
import com.syncbridge.colombodeals.common.Log;
import com.syncbridge.colombodeals.common.Preference;
import com.syncbridge.colombodeals.common.ProgressBar;
import com.syncbridge.colombodeals.common.ResponseErrorMessageHandler;
import com.syncbridge.colombodeals.interfaces.OnItemListener;
import com.syncbridge.colombodeals.models.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SubscribeActivity extends AppCompatActivity implements OnItemListener, MaterialDialog.SingleButtonCallback {

    private static final String TAG = SubscribeActivity.class.getSimpleName();
    private Context mContext;
    private ProgressBar mProgressBar;
    private ResponseErrorMessageHandler mREM;
    private User mUser;
    private MaterialEditText mEmail;

    //region functions

    private void subscribe(String email) {

        if (!Functions.IsInternetOn(mContext)) {
            mREM.showRetry(Functions.GetStringResource(mContext, R.string.error_internet_not_available), -1, null, null);
            return;
        }

        mProgressBar.start();

        ApiUsage.Subscribe(email, new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String jsonString = response.body().string();
                    Log.d(TAG, jsonString);

                    JSONObject mObject = new JSONObject(jsonString);

                    switch (mObject.getInt("status")) {
                        case 0:
                            Toast.makeText(mContext, mObject.getString("msg"), Toast.LENGTH_LONG).show();
                            break;
                        case 1:
                            Toast.makeText(mContext, mObject.getString("msg"), Toast.LENGTH_LONG).show();
                            break;
                    }

                } catch (IOException | JSONException e) {
                    Log.e(TAG, e.toString());
                    Toast.makeText(mContext, e.toString(), Toast.LENGTH_LONG).show();
                }

                mProgressBar.stop();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                mProgressBar.stop();
                Log.e(TAG, "authenticate", t.toString());
                Toast.makeText(mContext, t.toString(), Toast.LENGTH_LONG).show();
            }
        });
    }

    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscribe);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        mContext = SubscribeActivity.this;

        mProgressBar = ProgressBar.getInstance(findViewById(R.id.progress_container));
        mREM = new ResponseErrorMessageHandler(mContext, this);
        mREM.cancelable(false);
        mREM.showCancelButton(true);

        mUser = Preference.getInstance(mContext).getCurrentUser();

        mEmail = (MaterialEditText) findViewById(R.id.email);
        mEmail.addValidator(Functions.ValidationRequired(mContext));
        mEmail.addValidator(Functions.ValidationEmail(mContext));

        if (mUser != null && mUser.email != null && !mUser.email.isEmpty())
            mEmail.setText(mUser.email);
    }

    @Override
    public void onItemListener(View v) {
        if (v.getId() == R.id.subscribe) {
            if (mEmail.validate()) {
                subscribe(mEmail.getText().toString());
            }
        }
    }

    @Override
    public void onItemListener(View v, int region) {

    }

    @Override
    public void onItemListener(int position, Identity identity, Object object) {

    }

    @Override
    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
        switch (which) {
            case NEGATIVE:
                dialog.dismiss();
                break;
            case POSITIVE:
                dialog.dismiss();
                subscribe(mEmail.getText().toString());
                break;
        }
    }
}
