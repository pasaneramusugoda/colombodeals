package com.syncbridge.colombodeals.views.activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.syncbridge.colombodeals.R;
import com.syncbridge.colombodeals.adapters.DealAdapter;
import com.syncbridge.colombodeals.api.ApiUsage;
import com.syncbridge.colombodeals.common.Functions;
import com.syncbridge.colombodeals.common.Identity;
import com.syncbridge.colombodeals.common.Log;
import com.syncbridge.colombodeals.common.Preference;
import com.syncbridge.colombodeals.common.ProgressBar;
import com.syncbridge.colombodeals.common.ResponseErrorMessageHandler;
import com.syncbridge.colombodeals.common.SaveInstanceStates;
import com.syncbridge.colombodeals.interfaces.OnItemListener;
import com.syncbridge.colombodeals.models.Deal;
import com.syncbridge.colombodeals.models.Deals;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FilterResultActivity extends AppCompatActivity implements MaterialDialog.SingleButtonCallback, OnItemListener {

    private static final String TAG = FilterResultActivity.class.getSimpleName();
    private Context mContext;
    private ProgressBar mProgressBar;
    private ResponseErrorMessageHandler mREM;
    private List<Deal> mDeals = new ArrayList<>();
    private DealAdapter mDealAdapter;
    private String mMerchants;
    private String mStartDate;
    private String mEndDate;

    //region functions

    private void emptyResult() {
        new MaterialDialog.Builder(mContext)
                .content(R.string.dialog_message_empty_search_result)
                .positiveText(R.string.dialog_button_ok)
                .cancelable(true)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    private void searchDeals() {

        if (!Functions.IsInternetOn(mContext)) {
            mREM.showRetry(Functions.GetStringResource(mContext, R.string.error_internet_not_available), -1, null, null);
            return;
        }

        mProgressBar.start();

        ApiUsage.GetDeals(mMerchants, mStartDate, mEndDate, new Callback<Deals>() {
            @Override
            public void onResponse(Call<Deals> call, Response<Deals> response) {
                if (response.isSuccessful()) {
                    switch (response.body().getStatus()) {
                        case 1:
                            mDeals.addAll(response.body().getDeals());

                            HashSet<Deal> cleanedList = new LinkedHashSet<>(mDeals);
                            mDeals.clear();
                            mDeals.addAll(cleanedList);

                            mDealAdapter.notifyDataSetChanged();
                            break;
                        default:
                            Log.e(TAG, response.body().getMsg());
                            break;
                    }
                    mProgressBar.stop();

                    if(mDeals.isEmpty())
                        emptyResult();

                } else {
                    mProgressBar.stop();
                    mREM.showRetry(response.body().getMsg(), response.code(), null, null);
                }
            }

            @Override
            public void onFailure(Call<Deals> call, Throwable t) {
                Log.e(TAG, t.toString());
                mProgressBar.stop();
                mREM.showRetry(null, -1, t, null);
            }
        });
    }

    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_result);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        mContext = FilterResultActivity.this;
        mProgressBar = ProgressBar.getInstance(findViewById(R.id.progress_container));
        mREM = new ResponseErrorMessageHandler(mContext, this);
        mREM.cancelable(false);
        mREM.showCancelButton(true);

        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.rv_deals);
        if (mRecyclerView == null) {
            return;
        }

        mRecyclerView.setHasFixedSize(true);

        mDealAdapter = new DealAdapter(mContext, null, mDeals, this);
        mRecyclerView.setAdapter(mDealAdapter);

        StaggeredGridLayoutManager mGridLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mGridLayoutManager);

        handleIntent(getIntent());
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putString(SaveInstanceStates.MERCHANT.name(), mMerchants);
        savedInstanceState.putString(SaveInstanceStates.START_DATE.name(), mStartDate);
        savedInstanceState.putString(SaveInstanceStates.END_DATE.name(), mEndDate);

        super.onSaveInstanceState(savedInstanceState);
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        mMerchants = savedInstanceState.getString(SaveInstanceStates.MERCHANT.name());
        mStartDate = savedInstanceState.getString(SaveInstanceStates.START_DATE.name());
        mEndDate = savedInstanceState.getString(SaveInstanceStates.END_DATE.name());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {

        if (intent.hasExtra("merchants") && intent.hasExtra("startDate") && intent.hasExtra("endDate")) {
            mMerchants = intent.getStringExtra("merchants");
            mStartDate = intent.getStringExtra("startDate");
            mEndDate = intent.getStringExtra("endDate");
            searchDeals();
        }
    }

    @Override
    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
        switch (which) {
            case NEGATIVE:
                dialog.dismiss();
                break;
            case POSITIVE:
                dialog.dismiss();
                searchDeals();
                break;
        }
    }

    @Override
    public void onItemListener(View v) {

    }

    @Override
    public void onItemListener(View v, int region) {

    }

    @Override
    public void onItemListener(int position, Identity identity, Object object) {
        switch (identity) {
            case DEAL_ITEM:
                Deal deal = (Deal) object;

                Intent intent = new Intent(mContext, DealDetailActivity.class);
                intent.putExtra(Preference.Keys.DEAL_ID.name(), deal.getId());
                startActivity(intent);
                break;
            case DEAL_ITEM_SHARE:
                deal = (Deal) object;

                // Create a deep link and display it in the UI
                final Uri deepLink = Functions.BuildDeepLink(mContext,
                        Uri.parse(String.format(Locale.getDefault(),
                                Functions.GetStringResource(mContext, R.string.deep_link_url), deal.getId())),
                        0, false);
                Log.d(TAG, "Deep Link", deepLink.toString());

                Functions.ShareDeepLink(mContext, deal.getTitle(), deepLink.toString());
                break;
        }
    }
}
