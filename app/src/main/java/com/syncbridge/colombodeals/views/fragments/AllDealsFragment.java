package com.syncbridge.colombodeals.views.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.syncbridge.colombodeals.R;
import com.syncbridge.colombodeals.adapters.DealAdapter;
import com.syncbridge.colombodeals.adapters.SimpleSectionedRecyclerViewAdapter;
import com.syncbridge.colombodeals.api.ApiUsage;
import com.syncbridge.colombodeals.common.Functions;
import com.syncbridge.colombodeals.common.HidingScrollListener;
import com.syncbridge.colombodeals.common.Identity;
import com.syncbridge.colombodeals.common.Log;
import com.syncbridge.colombodeals.common.Preference;
import com.syncbridge.colombodeals.common.ProgressBar;
import com.syncbridge.colombodeals.common.ResponseErrorMessageHandler;
import com.syncbridge.colombodeals.common.SaveInstanceStates;
import com.syncbridge.colombodeals.interfaces.OnItemListener;
import com.syncbridge.colombodeals.models.Banner;
import com.syncbridge.colombodeals.models.Banners;
import com.syncbridge.colombodeals.models.Deal;
import com.syncbridge.colombodeals.models.Deals;
import com.syncbridge.colombodeals.views.activities.DealDetailActivity;
import com.syncbridge.colombodeals.views.activities.LoginActivity;

import net.servicestack.func.Func;
import net.servicestack.func.Predicate;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllDealsFragment extends Fragment implements OnItemListener, MaterialDialog.SingleButtonCallback {

    protected static final String TAG = AllDealsFragment.class.getSimpleName();
    private FirebaseAnalytics mFirebaseAnalytics;
    private Context mContext;
    private ProgressBar mProgressBar;
    private ResponseErrorMessageHandler mREM;
    private List<Banner> mBanners = new ArrayList<>();
    private List<Deal> mDeals = new ArrayList<>();
    private List<SimpleSectionedRecyclerViewAdapter.Section> mSections = new ArrayList<>();
    private DealAdapter mDealAdapter;
    private SimpleSectionedRecyclerViewAdapter mSectionedAdapter;
    private int mRequestLimit = 20;
    private int mOffsetValue = 0;

    public AllDealsFragment() {
        // Required empty public constructor
    }

    public static AllDealsFragment newInstance() {
        return new AllDealsFragment();
    }

    private void showUserLogin() {
        new MaterialDialog.Builder(mContext)
                .title(Functions.GetStringResource(mContext, R.string.dialog_title_error_user))
                .content(Functions.GetStringResource(mContext, R.string.dialog_message_user_login))
                .positiveText(R.string.dialog_button_ok)
                .negativeText(R.string.dialog_button_cancel)
                .cancelable(false)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        gotoLogin();
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    private void gotoLogin() {
        Intent intent = new Intent(mContext, LoginActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    private void getDeals(int offset) {

        if (!Functions.IsInternetOn(mContext)) {
            mREM.showRetry(Functions.GetStringResource(mContext, R.string.error_internet_not_available), -1, null, null);
            return;
        }

        if (offset == 0)
            mProgressBar.start();

        ApiUsage.GetDeals(mRequestLimit, offset, new Callback<Deals>() {
            @Override
            public void onResponse(Call<Deals> call, Response<Deals> response) {
                if (response.isSuccessful()) {
                    switch (response.body().getStatus()) {
                        case 1:
                            mDeals.addAll(response.body().getDeals());

                            HashSet<Deal> cleanedList = new LinkedHashSet<>(mDeals);
                            mDeals.clear();
                            mDeals.addAll(cleanedList);

                            int startPosition = 0;

                            if (mBanners != null && mBanners.size() > 0)
                                startPosition++;

                            List<Deal> featuredDeals = Func.filter(mDeals, new Predicate<Deal>() {
                                @Override
                                public boolean apply(Deal deal) {
                                    return Integer.parseInt(deal.getIsFeatured()) == 1;
                                }
                            });

                            List<Deal> noneFeaturedDeals = Func.filter(mDeals, new Predicate<Deal>() {
                                @Override
                                public boolean apply(Deal deal) {
                                    return Integer.parseInt(deal.getIsFeatured()) == 0;
                                }
                            });

                            if (featuredDeals.size() > 0 &&
                                    Func.filter(mSections, new Predicate<SimpleSectionedRecyclerViewAdapter.Section>() {
                                        @Override
                                        public boolean apply(SimpleSectionedRecyclerViewAdapter.Section section) {
                                            return section.getTitle().equals("Current Deals");
                                        }
                                    }).size() == 0) {
                                mSections.add(new SimpleSectionedRecyclerViewAdapter.Section(startPosition, "Current Deals"));
                            }

                            if (noneFeaturedDeals.size() > 0 &&
                                    Func.filter(mSections, new Predicate<SimpleSectionedRecyclerViewAdapter.Section>() {
                                        @Override
                                        public boolean apply(SimpleSectionedRecyclerViewAdapter.Section section) {
                                            return section.getTitle().equals("Deals");
                                        }
                                    }).size() == 0) {
                                mSections.add(new SimpleSectionedRecyclerViewAdapter.Section(startPosition + featuredDeals.size(), "Deals"));
                            }

                            SimpleSectionedRecyclerViewAdapter.Section[] dummy = new SimpleSectionedRecyclerViewAdapter.Section[mSections.size()];

                            mDealAdapter.notifyDataSetChanged();
                            mSectionedAdapter.setSections(mSections.toArray(dummy));
                            break;
                        default:
                            Log.e(TAG, response.body().getMsg() == null ? "An error occurred." : response.body().getMsg());
                            break;
                    }
                    mProgressBar.stop();
                } else {
                    mProgressBar.stop();
                    mREM.showRetry(response.body().getMsg() == null ? "An error occurred." : response.body().getMsg(), response.code(), null, null);
                }
            }

            @Override
            public void onFailure(Call<Deals> call, Throwable t) {
                Log.e(TAG, t.toString());
                mProgressBar.stop();
                mREM.showRetry(null, -1, t, null);
            }
        });
    }

    private void getBanners() {
        ApiUsage.GetBanners(new Callback<Banners>() {
            @Override
            public void onResponse(Call<Banners> call, Response<Banners> response) {
                if (response.isSuccessful()) {
                    switch (response.body().getStatus()) {
                        case 1:
                            mBanners.addAll(response.body().getBanners());
                            mDealAdapter.setBanners(mBanners);
                            break;
                        default:
                            Log.e(TAG, response.body().getMsg());
                            break;
                    }
                } else {
                    try {
                        Log.e(TAG, String.format(Locale.getDefault(), "StatusCode: %d \n ErrorMessage: %s", response.code(), response.message()));
                    } catch (Exception ignored) {
                    }
                }
            }

            @Override
            public void onFailure(Call<Banners> call, Throwable t) {
                Log.e(TAG, t.toString());
            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(SaveInstanceStates.OFFSET.name(), mOffsetValue);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (savedInstanceState != null) {
            mOffsetValue = savedInstanceState.getInt(SaveInstanceStates.OFFSET.name());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_all_deals, container, false);

        mProgressBar = ProgressBar.getInstance(mView.findViewById(R.id.progress_container));
        mREM = new ResponseErrorMessageHandler(mContext, this);
        mREM.cancelable(false);
        mREM.showCancelButton(true);

        getBanners();

        RecyclerView mRecyclerView = (RecyclerView) mView.findViewById(R.id.rv_all_deals);
        mRecyclerView.setHasFixedSize(true);

        StaggeredGridLayoutManager mGridLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mGridLayoutManager);

        mDealAdapter = new DealAdapter(mContext, mBanners, mDeals, this);

        //Add your adapter to the sectionAdapter
        SimpleSectionedRecyclerViewAdapter.Section[] dummy = new SimpleSectionedRecyclerViewAdapter.Section[mSections.size()];
        mSectionedAdapter = new SimpleSectionedRecyclerViewAdapter(mContext, R.layout.layout_deal_header_item, R.id.tvTitle, mDealAdapter);
        mSectionedAdapter.setSections(mSections.toArray(dummy));

        //Apply this adapter to the RecyclerView
        mRecyclerView.setAdapter(mSectionedAdapter);

        mRecyclerView.addOnScrollListener(new HidingScrollListener(mContext,
                Functions.GetToolbarHeight(mContext), mGridLayoutManager) {
            @Override
            public void onMoved(int distance) {
            }

            @Override
            public void onLoadMore(int current_page) {
                mOffsetValue = (current_page * mRequestLimit) + current_page;

                Log.d(TAG, String.format(Locale.getDefault(), "Offset value: %d", mOffsetValue));
                getDeals(mOffsetValue);
            }
        });

        getDeals(mOffsetValue);

        return mView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onItemListener(View v) {

    }

    @Override
    public void onItemListener(View v, int region) {

    }

    @Override
    public void onItemListener(int position, Identity identity, Object object) {

        if (Preference.getInstance(mContext).getCurrentUser() == null) {
            showUserLogin();
            return;
        }

        switch (identity) {
            case SLIDER:
                Banner banner = (Banner) object;
                Intent intent = new Intent(mContext, DealDetailActivity.class);
                intent.putExtra(Preference.Keys.DEAL_ID.name(), banner.getDealId());
                startActivity(intent);
                break;
            case DEAL_ITEM:
                Deal deal = (Deal) object;
                intent = new Intent(mContext, DealDetailActivity.class);
                intent.putExtra(Preference.Keys.DEAL_ID.name(), deal.getId());
                startActivity(intent);
                break;
            case DEAL_ITEM_SHARE:
                deal = (Deal) object;
                Log.d(TAG, "clicked deal item share:" + deal.getId());

                // Create a deep link and display it in the UI
                final Uri deepLink = Functions.BuildDeepLink(mContext,
                        Uri.parse(String.format(Locale.getDefault(),
                                Functions.GetStringResource(mContext, R.string.deep_link_url), deal.getId())),
                        0, false);
                Log.d(TAG, "Deep Link", deepLink.toString());

                Functions.ShareDeepLink(mContext, deal.getTitle(), deepLink.toString());

                Bundle params = new Bundle();
                params.putString("deal_id", deal.getId());
                params.putString("deal_title", deal.getTitle());
                mFirebaseAnalytics.logEvent("share_deal", params);
                break;
        }
    }

    @Override
    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
        switch (which) {
            case NEGATIVE:
                dialog.dismiss();
                break;
            case POSITIVE:
                dialog.dismiss();
                getBanners();
                getDeals(mOffsetValue);
                break;
        }
    }
}
