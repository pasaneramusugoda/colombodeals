package com.syncbridge.colombodeals.views.activities;

import android.Manifest;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rengwuxian.materialedittext.validation.METValidator;
import com.squareup.picasso.Picasso;
import com.syncbridge.colombodeals.R;
import com.syncbridge.colombodeals.api.ApiUsage;
import com.syncbridge.colombodeals.common.Functions;
import com.syncbridge.colombodeals.common.Log;
import com.syncbridge.colombodeals.common.Preference;
import com.syncbridge.colombodeals.common.ProgressBar;
import com.syncbridge.colombodeals.common.ResponseErrorMessageHandler;
import com.syncbridge.colombodeals.models.User;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyAccountActivity extends AppCompatActivity implements View.OnClickListener,
        MaterialDialog.SingleButtonCallback {

    private static final String TAG = MyAccountActivity.class.getSimpleName();
    private static final int SELECT_PICTURE = 1;
    private static final int PERMISSIONS_REQUEST_WRITE_TO_STORAGE = 865;

    private Context mContext;
    private ProgressBar mProgressBar;
    private ResponseErrorMessageHandler mREM;
    private Uri mOutputFileUri;

    private MaterialEditText mFullName, mEmail, mCurrentPassword, mNewPassword, mCNewPassword;
    private CircleImageView mProfileImageView;

    //region fetch details

    private void fetchData() {
        User mUser = Preference.getInstance(mContext).getCurrentUser();

        if (mUser != null) {
            if (mUser.profile_pic_url == null || mUser.profile_pic_url.isEmpty()) {
                if (mProfileImageView != null) {
                    mProfileImageView.setImageDrawable(
                            Functions.GetDrawable(mContext, R.mipmap.ic_launcher)
                    );
                }
            } else {
                Picasso.with(mContext)
                        .load(mUser.profile_pic_url)
                        .placeholder(Functions.GetDrawable(mContext, R.drawable.profile_picture))
                        .error(Functions.GetDrawable(mContext, R.mipmap.ic_launcher))
                        .into(mProfileImageView);
            }

            mFullName.setText(mUser.name);
            mEmail.setText(mUser.email);
        }
    }

    private void getUser() {

        final User mUser = Preference.getInstance(mContext).getCurrentUser();

        if (!Functions.IsInternetOn(mContext)) {
            mREM.showRetry(Functions.GetStringResource(mContext, R.string.error_internet_not_available), -1, null, null);
            return;
        }

        mProgressBar.start();

        ApiUsage.GetCustomer(mUser.customer_id, mUser.token, new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String jsonString = response.body().string();
                    Log.d(TAG, "GetUser", jsonString);

                    JSONObject mObject = new JSONObject(jsonString);

                    switch (mObject.getInt("status")) {
                        case 0:
                            Toast.makeText(mContext, mObject.getString("msg"), Toast.LENGTH_LONG).show();
                            break;
                        case 1:
                            JSONObject mProfile = mObject.getJSONObject("profile");

                            mUser.profile_pic_url = mProfile.getString("image");
                            mUser.name = mProfile.getString("name");

                            Preference.getInstance(mContext).setCurrentUser(mUser);

                            break;
                    }

                } catch (IOException | JSONException e) {
                    Log.e(TAG, "GetUser", e.toString());
                    Toast.makeText(mContext, e.toString(), Toast.LENGTH_LONG).show();
                }

                mProgressBar.stop();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                mProgressBar.stop();
                Log.e(TAG, "GetUser", t.toString());
                Toast.makeText(mContext, t.toString(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void updateProfile() {

        User mUser = Preference.getInstance(mContext).getCurrentUser();

        if (mFullName.validate() && mEmail.validate()) {
            if ((!mCurrentPassword.getText().toString().isEmpty() || !mCNewPassword.getText().toString().isEmpty() || !mNewPassword.getText().toString().isEmpty())
                    && (!mNewPassword.validate() || !mCNewPassword.validate() || !mCurrentPassword.validate())) {
                return;
            }

            if (!Functions.IsInternetOn(mContext)) {
                mREM.showRetry(Functions.GetStringResource(mContext, R.string.error_internet_not_available), -1, null, null);
                return;
            }

            mProgressBar.start();

            String mEncodedImageString = "";

            if (mOutputFileUri != null) {
                try {
                    Log.d(TAG, "image path", mOutputFileUri.getPath());
                    File file = new File(mOutputFileUri.getPath());

                    // Reading a Image file from file system
                    FileInputStream imageInFile = new FileInputStream(file);
                    byte imageData[] = new byte[(int) file.length()];
                    imageInFile.read(imageData);

                    // Converting Image byte array into Base64 String
                    mEncodedImageString = Functions.EncodeBase64(imageData);

                    imageInFile.close();
                    Log.d(TAG, "EncodedImageString", mEncodedImageString);

                } catch (FileNotFoundException e) {
                    Log.e(TAG, e.toString());
                    Toast.makeText(mContext, "Image not found " + e, Toast.LENGTH_LONG).show();
                    if(mEncodedImageString.isEmpty())
                        return;
                } catch (IOException ioe) {
                    Log.e(TAG, ioe.toString());
                    Toast.makeText(mContext, "Exception while reading the Image " + ioe, Toast.LENGTH_LONG).show();
                    if(mEncodedImageString.isEmpty())
                        return;
                } catch (OutOfMemoryError oex) {
                    Log.e(TAG, oex.toString());
                    Toast.makeText(mContext, "OutOfMemoryError " + oex.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                    if(mEncodedImageString.isEmpty())
                        return;
                }
            }

            ApiUsage.UserUpdate(mUser.customer_id, mFullName.getText().toString(),
                    mCurrentPassword.getText().toString(), mNewPassword.getText().toString(),
                    mEncodedImageString, mUser.token, new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            try {
                                String jsonString = response.body().string();
                                Log.d(TAG, "UserUpdate", jsonString);

                                JSONObject mObject = new JSONObject(jsonString);

                                switch (mObject.getInt("status")) {
                                    case 0:
                                        Toast.makeText(mContext, mObject.getString("msg"), Toast.LENGTH_LONG).show();
                                        break;
                                    case 1:
                                        Toast.makeText(mContext, mObject.getString("msg"), Toast.LENGTH_LONG).show();
                                        getUser();
                                        break;
                                }

                            } catch (IOException | JSONException e) {
                                Log.e(TAG, "UserUpdate", e.toString());
                                Toast.makeText(mContext, e.toString(), Toast.LENGTH_LONG).show();
                            }

                            mProgressBar.stop();
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            mProgressBar.stop();
                            Log.e(TAG, "UserUpdate", t.toString());
                            Toast.makeText(mContext, t.toString(), Toast.LENGTH_LONG).show();
                        }
                    });
        }
    }

    private void openImageIntent() {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
            ActivityCompat.requestPermissions(MyAccountActivity.this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSIONS_REQUEST_WRITE_TO_STORAGE);
        } else {
            mProgressBar.start();

            // Determine Uri of camera image to save.
            final File root = new File(Environment.getExternalStorageDirectory() + File.separator + "MyDir" + File.separator);
            root.mkdirs();

            String fname = "";
            try {
                fname = File.createTempFile("prf_img", "").getName();
            } catch (IOException e) {
                Toast.makeText(mContext, e.toString(), Toast.LENGTH_LONG).show();
                return;
            }

            final File sdImageMainDirectory = new File(root, fname);
            mOutputFileUri = Uri.fromFile(sdImageMainDirectory);

            // Camera.
            final List<Intent> cameraIntents = new ArrayList<Intent>();
            final Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            final PackageManager packageManager = getPackageManager();
            final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
            for (ResolveInfo res : listCam) {
                final String packageName = res.activityInfo.packageName;
                final Intent intent = new Intent(captureIntent);
                intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
                intent.setPackage(packageName);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, mOutputFileUri);
                cameraIntents.add(intent);
            }

            // Filesystem.
            final Intent galleryIntent = new Intent();
            galleryIntent.setType("image/*");
            galleryIntent.setAction(Intent.ACTION_GET_CONTENT);

            // Chooser of filesystem options.
            final Intent chooserIntent = Intent.createChooser(galleryIntent, "Select Source");

            // Add the camera options.
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[cameraIntents.size()]));

            startActivityForResult(chooserIntent, SELECT_PICTURE);

            mProgressBar.stop();
        }
    }

    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_account);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        mContext = MyAccountActivity.this;

        mProgressBar = ProgressBar.getInstance(findViewById(R.id.progress_container));
        mREM = new ResponseErrorMessageHandler(mContext, this);
        mREM.cancelable(false);
        mREM.showCancelButton(true);

        mFullName = (MaterialEditText) findViewById(R.id.name);
        mEmail = (MaterialEditText) findViewById(R.id.email);
        mCurrentPassword = (MaterialEditText) findViewById(R.id.current_password);
        mNewPassword = (MaterialEditText) findViewById(R.id.new_password);
        mCNewPassword = (MaterialEditText) findViewById(R.id.c_new_password);
        mProfileImageView = (CircleImageView) findViewById(R.id.profile_image);

        mFullName.addValidator(Functions.ValidationRequired(mContext));

        mCurrentPassword.addValidator(Functions.ValidationRequired(mContext));
        mNewPassword.addValidator(Functions.ValidationRequired(mContext));
        mCNewPassword.addValidator(Functions.ValidationRequired(mContext));
        mCNewPassword.addValidator(new METValidator(Functions.GetStringResource(mContext, R.string.error_password_not_match)) {
            @Override
            public boolean isValid(@NonNull CharSequence text, boolean isEmpty) {
                return !isEmpty && Functions.IsValidPassword(mNewPassword.getText().toString(), text);
            }
        });

        if (findViewById(R.id.btn_update) != null)
            findViewById(R.id.btn_update).setOnClickListener(this);

        fetchData();
    }

    @Override
    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        mProgressBar.start();
        if (requestCode == SELECT_PICTURE) {
            if (resultCode == RESULT_OK) {
                final boolean isCamera;
                if (data == null) {
                    isCamera = true;
                } else {
                    final String action = data.getAction();
                    if (action == null) {
                        isCamera = false;
                    } else {
                        isCamera = action.equals(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    }
                }

                Uri selectedImageUri;
                if (isCamera) {
                    selectedImageUri = mOutputFileUri;
                } else {
                    selectedImageUri = data.getData();
                }

                CropImage.activity(selectedImageUri)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(this);
            }
        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                mOutputFileUri = resultUri;
                mProfileImageView.setImageURI(resultUri);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Toast.makeText(mContext, error.toString(), Toast.LENGTH_LONG).show();
            }
        }
        mProgressBar.stop();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_update:
                updateProfile();
                break;
            case R.id.add_image:
                openImageIntent();
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_WRITE_TO_STORAGE) {
            if (permissions.length > 0 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                openImageIntent();
            } else {
                Toast.makeText(mContext, "Permission denied.", Toast.LENGTH_LONG).show();
            }
        }
    }
}
