package com.syncbridge.colombodeals.views.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.facebook.login.LoginManager;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.syncbridge.colombodeals.R;
import com.syncbridge.colombodeals.adapters.SectionsPagerAdapter;
import com.syncbridge.colombodeals.api.ApiUsage;
import com.syncbridge.colombodeals.common.Functions;
import com.syncbridge.colombodeals.common.Identity;
import com.syncbridge.colombodeals.common.Log;
import com.syncbridge.colombodeals.common.Preference;
import com.syncbridge.colombodeals.common.ProgressBar;
import com.syncbridge.colombodeals.common.ResponseErrorMessageHandler;
import com.syncbridge.colombodeals.interfaces.NavigationDrawerCallbacks;
import com.syncbridge.colombodeals.interfaces.OnItemListener;
import com.syncbridge.colombodeals.models.MainNavItems;
import com.syncbridge.colombodeals.models.User;
import com.syncbridge.colombodeals.views.fragments.MainNavFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements OnItemListener, NavigationDrawerCallbacks,
        MaterialDialog.SingleButtonCallback, ViewPager.OnPageChangeListener {
    private static final String TAG = MainActivity.class.getSimpleName();
    private static FirebaseAnalytics mFirebaseAnalytics;
    private Context mContext;
    private ProgressBar mProgressBar;
    private ResponseErrorMessageHandler mREM;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private ImageView mToolBarLogo;
    private TabLayout mTabLayout;

    //region functions

    private void showUserLogin() {
        gotoLogin();
//        new MaterialDialog.Builder(mContext)
//                .title(Functions.GetStringResource(mContext, R.string.dialog_title_error_user))
//                .content(Functions.GetStringResource(mContext, R.string.dialog_message_user_login))
//                .positiveText(R.string.dialog_button_ok)
//                .negativeText(R.string.dialog_button_cancel)
//                .cancelable(false)
//                .onPositive(new MaterialDialog.SingleButtonCallback() {
//                    @Override
//                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//                        gotoLogin();
//                    }
//                })
//                .onNegative(new MaterialDialog.SingleButtonCallback() {
//                    @Override
//                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//                        mViewPager.setCurrentItem(0, true);
//                    }
//                })
//                .show();
    }

    private void gotoLogin() {
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void logoutUser() {

        if (!Functions.IsInternetOn(mContext)) {
            mREM.showRetry(Functions.GetStringResource(mContext, R.string.error_internet_not_available), -1, null, null);
            return;
        }

        final User mUser = Preference.getInstance(mContext).getCurrentUser();

        if (mUser.customer_id == null || mUser.customer_id.isEmpty() || mUser.token == null || mUser.token.isEmpty()) {
            Toast.makeText(mContext, "User information not available.", Toast.LENGTH_LONG).show();
            return;
        }

        mProgressBar.start();

        ApiUsage.UserLogout(mUser.customer_id, mUser.token, new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String jsonString = response.body().string();
                    Log.d(TAG, jsonString);

                    JSONObject mObject = new JSONObject(jsonString);

                    switch (mObject.getInt("status")) {
                        case 0:
                            mProgressBar.stop();
                            Toast.makeText(mContext, mObject.getString("msg"), Toast.LENGTH_LONG).show();
                            break;
                        case 1:
                            switch (mUser.login_type) {
                                case FB:
                                    LoginManager.getInstance().logOut();
                                    break;
                                case GOOGLE:
                                    //region google
                                    //TODO: need to handle
//                Auth.GoogleSignInApi.signOut(mGoogleApiClient);
                                    //endregion
                                    break;
                            }

                            Preference.getInstance(mContext).clearCurrentUser();
                            mProgressBar.stop();
                            gotoLogin();
                            break;
                    }
                } catch (IOException | JSONException e) {
                    mProgressBar.stop();
                    Log.e(TAG, e.toString());
                    Toast.makeText(mContext, e.toString(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, t.toString());
                mProgressBar.stop();
                mREM.showRetry(null, -1, t, null);
            }
        });
    }

    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle("");

        mContext = MainActivity.this;

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        mProgressBar = ProgressBar.getInstance(findViewById(R.id.progress_container));
        mREM = new ResponseErrorMessageHandler(mContext, this);
        mREM.cancelable(false);
        mREM.showCancelButton(true);

        mToolBarLogo = (ImageView) findViewById(R.id.toolbar_logo);
        mTabLayout = (TabLayout) findViewById(R.id.tabs);

        mSectionsPagerAdapter = new SectionsPagerAdapter(mContext, getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.container);

        if (mViewPager != null) {
            mViewPager.setAdapter(mSectionsPagerAdapter);
            mViewPager.addOnPageChangeListener(this);
        }

        mTabLayout.setupWithViewPager(mViewPager);

        // Iterate over all tabs and set the custom view
        for (int i = 0; i < mTabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = mTabLayout.getTabAt(i);
            if (tab != null && mSectionsPagerAdapter != null)
                tab.setCustomView(mSectionsPagerAdapter.getTabView(i));
        }

        MainNavFragment mMainNavDrawerFragment = (MainNavFragment) getFragmentManager().findFragmentById(R.id.fragment_drawer);
        mMainNavDrawerFragment.setup(this, this, (DrawerLayout) findViewById(R.id.main_drawer), toolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);

        if(menu != null) {
            SearchView searchView =
                    (SearchView) menu.findItem(R.id.action_search).getActionView();
            searchView.setSearchableInfo(
                    searchManager.getSearchableInfo(getComponentName()));
        } else {
            recreate();
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == R.id.action_location) {
            if (Preference.getInstance(mContext).getCurrentUser() == null) {
                showUserLogin();
            } else {
                Bundle bundle = new Bundle();
                bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "action_location");
                mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);

                Intent intent = new Intent(MainActivity.this, MapsActivity.class);
                startActivity(intent);
            }
            return true;
        }

        if(id == R.id.action_filter) {
            if (Preference.getInstance(mContext).getCurrentUser() == null) {
                showUserLogin();
            } else {
                Intent intent = new Intent(MainActivity.this, FilterActivity.class);
                startActivity(intent);
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (MainNavFragment.isDrawerOpen()) {
            MainNavFragment.closeDrawer();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        if (getCurrentFocus() != null) {
            InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
        Intent intent = null;
        switch (position) {
            case MainNavItems.MY_ACCOUNT.id:
                intent = new Intent(MainActivity.this, MyAccountActivity.class);
                break;
            case MainNavItems.SUBSCRIBE.id:
                intent = new Intent(MainActivity.this, SubscribeActivity.class);
                break;
            case MainNavItems.ABOUT_US.id:
                intent = new Intent(MainActivity.this, AboutActivity.class);
                break;
            case MainNavItems.CONTACT_US.id:
                intent = new Intent(MainActivity.this, ContactUsActivity.class);
                break;
            case MainNavItems.NOTIFICATION.id:
                intent = new Intent(MainActivity.this, NotificationActivity.class);
                break;
            case MainNavItems.LOG_OUT.id:
                logoutUser();
                break;
        }

        if (intent != null) {
            startActivity(intent);
        }
    }

    @Override
    public void onItemListener(View v) {
        switch (v.getId()) {
            case R.id.sign_in:
                gotoLogin();
                break;
        }
    }

    @Override
    public void onItemListener(View v, int region) {

    }

    @Override
    public void onItemListener(int position, Identity identity, Object object) {

    }

    @Override
    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
        switch (which) {
            case NEGATIVE:
                dialog.dismiss();
                break;
            case POSITIVE:
                logoutUser();
                break;
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        if (position > 0 && Preference.getInstance(mContext).getCurrentUser() == null) {
            showUserLogin();
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    protected void onResume() {
        super.onResume();

        User user = Preference.getInstance(getApplicationContext()).getCurrentUser();
        if (!Preference.getInstance(getApplicationContext()).get(Preference.Keys.INSTANCE_ID_SENT_TO_SERVER, false)
                && user != null) {
            String token = FirebaseInstanceId.getInstance().getToken();

            ApiUsage.RegisterDevice(user.customer_id, token, new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                    try {
                        String jsonString = response.body().string();
                        Log.d(TAG, jsonString);
                        JSONObject mObject = new JSONObject(jsonString);
                        switch (mObject.getInt("status")) {
                            case 1:
                                Preference.getInstance(getApplicationContext()).write(Preference.Keys.INSTANCE_ID_SENT_TO_SERVER, true);
                                break;
                            case 0:
                                Preference.getInstance(getApplicationContext()).write(Preference.Keys.INSTANCE_ID_SENT_TO_SERVER, false);
                                Log.e(TAG, "RegisterDevice", mObject.getString("msg"));
                                break;
                        }
                    } catch (NullPointerException e) {
                        FirebaseCrash.report(e);
                    } catch (IOException | JSONException e) {
                        Preference.getInstance(getApplicationContext()).write(Preference.Keys.INSTANCE_ID_SENT_TO_SERVER, false);
                        Log.e(TAG, e.toString());
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Preference.getInstance(getApplicationContext()).write(Preference.Keys.INSTANCE_ID_SENT_TO_SERVER, false);
                    Log.e(TAG, "sendRegistrationToServer", call.toString(), t);
                }
            });

            Log.d(TAG, "##TOKEN##", token);
            FirebaseMessaging.getInstance().unsubscribeFromTopic(getString(R.string.firebase_messaging_topic_unregistered));
        } else {
            FirebaseMessaging.getInstance().subscribeToTopic(getString(R.string.firebase_messaging_topic_unregistered));
        }
    }
}
